package au.com.aj2019.daggerDI

import android.content.Context

/**
 * Created by JoupleDesigner on 3/12/2018.
 */
object DaggerStrike {
    private var injector: Injector? = null


    fun doInjection(context: Context?): Injector {

        if (injector == null) {
            injector = DaggerInjector.builder().dependencyProvider(DependencyProvider(context)).build()
        }
        return injector!!
    }
}