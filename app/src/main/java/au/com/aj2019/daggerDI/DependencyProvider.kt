package au.com.aj2019.daggerDI

import android.content.Context
import au.com.aj2019.api.ApiScout
import au.com.aj2019.utilities.ViewUtils

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by JoupleDesigner on 3/12/2018.
 */
@Module
class DependencyProvider(private val context: Context?) {

    @Provides
    fun getScoutsApi() = ApiScout()

    @Provides
    fun getGSON(): Gson {
        return Gson()
    }

    @Provides
    fun viewUtils(): ViewUtils = ViewUtils(context!!)

    @Provides
    fun getRetrofit(): Retrofit {
        val baseURL = "http://103.219.145.21/api/v2/"

        val client = OkHttpClient().newBuilder().readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS).build()


        return Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
    }

}