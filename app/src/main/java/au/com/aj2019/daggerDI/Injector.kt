package au.com.aj2019.daggerDI

import au.com.aj2019.activities.Base
import au.com.aj2019.adapters.AppViewHolder
import au.com.aj2019.api.ApiScout
import dagger.Component
import javax.inject.Singleton

/**
 * Created by JoupleDesigner on 3/12/2018.
 */
@Singleton
@Component(modules = [DependencyProvider::class])
interface Injector {
    fun inject(apiScout: ApiScout)
    fun inject(base: Base)
    fun inject(appViewHolder: AppViewHolder)
}