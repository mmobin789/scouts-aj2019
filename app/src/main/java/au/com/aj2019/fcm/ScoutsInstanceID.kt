package au.com.aj2019.fcm

import android.util.Log
import au.com.aj2019.utilities.AppPersistence
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService


/**
 * Created by macbook on 29/01/2018.
 */
class ScoutsInstanceID : FirebaseInstanceIdService() {


    override fun onTokenRefresh() {
        // Get updated InstanceID token.
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d(javaClass.simpleName, "Refreshed token: " + refreshedToken!!)
        AppPersistence.saveFireBaseToken(this, refreshedToken)


        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //   sendRegistrationToServer(refreshedToken)

    }


}