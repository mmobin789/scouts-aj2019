package au.com.aj2019.fcm

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.support.v4.app.NotificationCompat
import android.util.Log
import au.com.aj2019.R

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

/**
 * Created by macbook on 29/01/2018.
 */
class ScoutsFCM : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage?) {
        sendNotification(p0?.notification)
    }

    private fun sendNotification(notification: RemoteMessage.Notification?) {
        val resultIntent = Intent(this, au.com.aj2019.activities.SplashActivity::class.java)

// Because clicking the notification opens a new ("special") activity, there's
// no need to create an artificial back stack.
        val resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                )

        val builder = NotificationCompat.Builder(this, "").setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.app_name)).setContentText(notification?.body)
                .setContentIntent(resultPendingIntent)
// Sets an ID for the notification
        val mNotificationId = 0
// Gets an instance of the NotificationManager service
        val mNotifyMgr = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
// Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, builder.build())
        Log.i("NotificationSent", notification.toString())
    }
}