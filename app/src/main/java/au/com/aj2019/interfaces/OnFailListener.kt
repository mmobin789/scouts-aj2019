package au.com.aj2019.interfaces

/**
 * Created by JoupleDesigner on 3/21/2018.
 */
interface OnFailListener {
    fun onFailed(errorCode: Int)
}