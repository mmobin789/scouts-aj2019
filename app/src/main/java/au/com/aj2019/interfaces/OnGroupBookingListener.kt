package au.com.aj2019.interfaces

import au.com.aj2019.models.BookingGroup

/**
 * Created by JoupleDesigner on 3/26/2018.
 */
interface OnGroupBookingListener : OnFailListener {
    fun onBookings(activityBookings: List<BookingGroup.Data.ActivityBooking>)
}