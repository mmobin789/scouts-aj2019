package au.com.aj2019.interfaces

import au.com.aj2019.models.Review

interface OnReviewsListener : OnFailListener {
    fun onReviewsListed(reviews: MutableList<Review.Data.ReviewData>)
}