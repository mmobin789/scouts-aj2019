package au.com.aj2019.interfaces

import au.com.aj2019.models.Review

interface OnRatingListener : OnFailListener {
    fun onRatingSent(reviewData: Review.Data.ReviewData)
}