package au.com.aj2019.interfaces

import au.com.aj2019.models.Booked


/**
 * Created by macbook on 02/02/2018.
 */
interface OnBookingListener : OnFailListener {
    fun onBookingSuccess(booked: Booked)
}