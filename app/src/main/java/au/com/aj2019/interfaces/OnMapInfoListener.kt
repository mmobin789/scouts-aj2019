package au.com.aj2019.interfaces

import au.com.aj2019.models.MapInfo


/**
 * Created by macbook on 08/02/2018.
 */
interface OnMapInfoListener {
    fun onMapInfo(mapInfo: MapInfo)
}