package au.com.aj2019.interfaces

import au.com.aj2019.models.ActivityPeriodGroup


/**
 * Created by JoupleDesigner on 3/12/2018.
 */
interface OnActivityPeriodGroupsListener : OnFailListener {
    fun onActivityPeriodGroups(activityPeriodGroups: List<ActivityPeriodGroup.Data.ActivityPeriodGroupsItem>)
}