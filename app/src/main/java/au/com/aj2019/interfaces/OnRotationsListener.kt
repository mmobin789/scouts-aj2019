package au.com.aj2019.interfaces

import au.com.aj2019.models.RotationGroup


/**
 * Created by JoupleDesigner on 3/21/2018.
 */
interface OnRotationsListener : OnFailListener {
    fun onUserRotations(activityRotations: List<RotationGroup.Data.ActivityRotation>)
}