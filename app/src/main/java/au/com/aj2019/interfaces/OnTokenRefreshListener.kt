package au.com.aj2019.interfaces

import au.com.aj2019.models.Auth


/**
 * Created by JoupleDesigner on 3/21/2018.
 */
interface OnTokenRefreshListener : OnFailListener {
    fun onTokenRefreshed(auth: Auth)

}