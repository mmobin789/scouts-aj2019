package au.com.aj2019.interfaces

/**
 * Created by MohammedMobinMunir on 3/20/2018.
 */
interface OnLogOutListener {
    fun onLogOutSuccess()
}