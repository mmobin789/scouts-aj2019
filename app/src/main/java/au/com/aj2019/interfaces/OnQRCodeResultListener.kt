package au.com.aj2019.interfaces

import au.com.aj2019.models.QRCodeResult


/**
 * Created by macbook on 08/02/2018.
 */
interface OnQRCodeResultListener {

    fun onQRCodeResult(qrCodeResult: QRCodeResult?)
}