package au.com.aj2019.interfaces

import au.com.aj2019.adapters.AppViewHolder


/**
 * Created by macbook on 02/01/2018.
 */
interface OnListItemClickListener {
    fun onItemClick(position: Int, holder: AppViewHolder)
}