package au.com.aj2019.interfaces

import au.com.aj2019.models.Auth


/**
 * Created by macbook on 01/01/2018.
 */
interface OnLoginListener {
    fun onLoginSuccess(auth: Auth)

    fun onLoginFail(error: String)


}