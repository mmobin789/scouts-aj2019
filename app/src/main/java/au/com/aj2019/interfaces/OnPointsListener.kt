package au.com.aj2019.interfaces

import au.com.aj2019.models.Points


/**
 * Created by macbook on 12/02/2018.
 */
interface OnPointsListener : OnFailListener {
    fun onPoints(points: Points)
}