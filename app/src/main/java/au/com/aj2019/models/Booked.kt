package au.com.aj2019.models

import com.google.gson.annotations.SerializedName

/**
 * Created by macbook on 02/02/2018.
 */
data class Booked(
        val data: Data? = null
) {
    data class Data(
            val booking: Booking
    ) {
        data class Booking(
                val id: Int,
                @SerializedName("group_id")
                val groupID: Int,
                @SerializedName("created_at")
                val createdAt: String,
                @SerializedName("activity_slot")
                val slot: Slot

        )


    }
}