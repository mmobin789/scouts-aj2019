package au.com.aj2019.models

import com.google.gson.annotations.SerializedName

data class User(

        @SerializedName("email")
        val email: String = "",
        val dob: String = "",

        @SerializedName("username")
        val username: String = "",

        @SerializedName("first_name")
        val first_name: String = "",

        @SerializedName("last_login")
        val last_login: Int? = -1,
        val id: Int = -1,
        val application_id: String = "",

        @SerializedName("user_Flags")
        val userFlags: String = "",

        @SerializedName("last_name")
        val last_name: String = "",
        @SerializedName("primary_group")
        val primaryGroup: PrimaryGroup,
        val groups: List<PrimaryGroup> = emptyList()
) {
    data class PrimaryGroup(val id: Int, @SerializedName("aj_id") val ajID: String, val name: String, val type: String)
}