package au.com.aj2019.models

/**
 * Created by JoupleDesigner on 3/26/2018.
 */
data class UserSlot(val rotation: RotationGroup.Data.ActivityRotation) {
    var rotationHasBooking = false
    var activitySlots: List<Slot>? = null


}