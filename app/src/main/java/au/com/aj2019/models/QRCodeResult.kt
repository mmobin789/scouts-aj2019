package au.com.aj2019.models

import com.google.gson.annotations.SerializedName

data class QRCodeResult(

        @field:SerializedName("data")
        val data: Any?,

        @field:SerializedName("message")
        val message: String? = "",

        @field:SerializedName("errors")
        val errors: String = "",

        @field:SerializedName("status")
        val status: Boolean = false
)