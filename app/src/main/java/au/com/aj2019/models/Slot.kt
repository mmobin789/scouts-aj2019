package au.com.aj2019.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Slot(
        @field:SerializedName("activity_period")
        val activityPeriod: ActivityPeriod,

        @field:SerializedName("bookable")
        val bookable: Boolean,

        @field:SerializedName("activity")
        val activity: Activity,

        @field:SerializedName("activity_site_id")
        val activitySiteId: Int,

        @field:SerializedName("booked_slots")
        val bookedSlots: Int,

        @field:SerializedName("id")
        val id: Int,

        @field:SerializedName("allocated_slots")
        val allocatedSlots: Int,

        @field:SerializedName("activity_rotation")
        val rotation: Rotation
) : Parcelable {
    @IgnoredOnParcel
    var isBooked = false
}