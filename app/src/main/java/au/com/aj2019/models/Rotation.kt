package au.com.aj2019.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Rotation(

        @field:SerializedName("activity_site_id")
        val activitySiteId: Int,

        @field:SerializedName("name")
        val name: String,

        @field:SerializedName("id")
        val id: Int,

        @field:SerializedName("order_id")
        val orderId: Int
) : Parcelable