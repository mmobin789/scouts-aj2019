package au.com.aj2019.models

import com.google.android.gms.maps.model.LatLng


data class MapInfo(


        var data: List<DataItem> = emptyList(),


        var message: String? = "",


        var errors: String? = "",

        var status: Boolean = false
) {
    data class DataItem(


            val colour: String? = "",


            val transparency: Int? = -1,

            val name: String? = "",


            val coOrdinates: List<LatLng> = emptyList()
    )

}

