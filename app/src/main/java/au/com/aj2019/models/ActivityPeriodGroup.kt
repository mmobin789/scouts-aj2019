package au.com.aj2019.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ActivityPeriodGroup(
        val data: Data

//        @field:SerializedName("error")
//        val error: Any? = null
//
//        @field:SerializedName("errors")
//        val errors: List<Any?>? = null
) : Parcelable {
    @Parcelize
    data class Data(

            @field:SerializedName("activity_period_groups")
            val activityPeriodGroups: List<ActivityPeriodGroupsItem> = emptyList()
    ) : Parcelable {
        @Parcelize
        data class ActivityPeriodGroupsItem(

                @field:SerializedName("end_at")
                val endAt: String = "",

                @field:SerializedName("updated_at")
                val updatedAt: String = "",

                @field:SerializedName("activity_periods")
                val activityPeriods: List<ActivityPeriod> = emptyList(),

//               @field:SerializedName("name")
                val name: String = "",

                //       @field:SerializedName("description")
                val description: String = "",

                @field:SerializedName("created_at")
                val createdAt: String = "",

//                @field:SerializedName("id")
                val id: Int = -1,

                @field:SerializedName("start_at")
                val startAt: String = ""
        ) : Parcelable {
            @Parcelize
            data class ActivityPeriod(

                    @field:SerializedName("end_at")
                    val endAt: String = "",

                    @field:SerializedName("activity_period_group_id")
                    val activityPeriodGroupId: Int = -1,

                    @field:SerializedName("updated_at")
                    val updatedAt: String = "",

                    //                  @field:SerializedName("name")
                    val name: String = "",

                    //           @field:SerializedName("description")
                    val description: String = "",

                    @field:SerializedName("created_at")
                    val createdAt: String = "",

                    @field:SerializedName("id")
                    val id: Int = -1,

                    @field:SerializedName("start_at")
                    val startAt: String = "",
                    @field:SerializedName("activity_site")
                    val activitySite: ActivitySite?
            ) : Parcelable {
                @Parcelize
                data class ActivitySite(val id: Int, val name: String, val description: String,
                                        @field:SerializedName("created_at")
                                        val createdAt: String = "",
                                        @field:SerializedName("updated_at")
                                        val updatedAt: String = "") : Parcelable
            }
        }


    }
}