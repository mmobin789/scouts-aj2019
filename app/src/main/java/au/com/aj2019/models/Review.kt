package au.com.aj2019.models

import com.google.gson.annotations.SerializedName

data class Review(
        @field:SerializedName("data")
        val data: Data) {

    data class Data(@field:SerializedName("reviews") val reviews: List<ReviewData>) {

        data class ReviewData(
                val rating: Int?,
                val comments: String?,
                val id: Int,
                @SerializedName("user_id")
                val uid: Int,
                @SerializedName("created_at")
                val createdAt: String,
                @SerializedName("updated_at")
                val updatedAt: String,
                @SerializedName("activity_slot")
                val activitySlot: Slot

        )
    }
}