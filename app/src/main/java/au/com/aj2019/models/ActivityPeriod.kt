package au.com.aj2019.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ActivityPeriod(

        @field:SerializedName("end_at")
        val endAt: String,

        @field:SerializedName("activity_period_group_id")
        val activityPeriodGroupId: Int,

        @field:SerializedName("updated_at")
        val updatedAt: String,

        @field:SerializedName("name")
        val name: String,

        @field:SerializedName("description")
        val description: String,

        @field:SerializedName("created_at")
        val createdAt: String,

        @field:SerializedName("id")
        val id: Int,

        @field:SerializedName("start_at")
        val startAt: String
) : Parcelable