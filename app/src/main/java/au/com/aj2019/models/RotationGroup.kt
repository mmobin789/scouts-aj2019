package au.com.aj2019.models


import com.google.gson.annotations.SerializedName

data class RotationGroup(
//
//        @field:SerializedName("error_message")
//        val errorMessage: Any? = null,

        @field:SerializedName("data")
        val data: Data

//        @field:SerializedName("error")
//        val error: Any? = null,
//
//        @field:SerializedName("errors")
//        val errors: List<Any?>? = null
) {
    data class Data(

            @field:SerializedName("activity_rotations")
            val activityRotations: List<ActivityRotation>
    ) {
        data class ActivityRotation(

                @field:SerializedName("activity_site_id")
                val activitySiteId: Int,

                @field:SerializedName("name")
                val name: String,

                @field:SerializedName("id")
                val id: Int,

                @field:SerializedName("order_id")
                val orderId: Int,

                @field:SerializedName("activity_slots")
                val activitySlots: List<Slot>?
        )


    }
}