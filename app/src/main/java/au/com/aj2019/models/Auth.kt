package au.com.aj2019.models

import au.com.aj2019.utilities.AppPersistence
import com.google.gson.annotations.SerializedName

/**
 * Created by macbook on 01/01/2018.
 */
data class Auth(@SerializedName("username") val name: String, val password: String) {

    val device = Device(AppPersistence.fireBaseToken)
    var status = ""
    var error = ""
    var message = ""

    val data: Data? = null

    data class Data(
            val user: User,
            val token: String = "",
            @SerializedName("expires_at")
            val expiry: String,
            val devices: List<Device>
    )

}