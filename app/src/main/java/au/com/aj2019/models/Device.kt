package au.com.aj2019.models

import android.os.Build
import com.google.gson.annotations.SerializedName

/**
 * Created by JoupleDesigner on 3/28/2018.
 */
class Device(fireBaseToken: String) {
    @SerializedName("device_id")
    val deviceID: String = fireBaseToken
    @SerializedName("os_version")
    val osVersion: String = Build.VERSION.RELEASE
    @SerializedName("apiVersion")
    val apiVersion: String = Build.VERSION.SDK_INT.toString()
    @SerializedName("hardware")
    val deviceInfo: String = getDevice() // like Nokia N6

    @SerializedName("app_version")
    val appVersion = "1.0"
    val os = "android"


//
//    private fun getStorageSize(): Long {
//        val stat = StatFs(Environment.getExternalStorageDirectory().path)
//        val bytesAvailable = if (Build.VERSION.SDK_INT >= 18) stat.blockSizeLong * stat.blockCountLong
//        else (stat.blockSize * stat.blockCount).toLong()
//        return bytesAvailable / 1048576
//    }

    private fun getDevice(): String =
            (Build.MANUFACTURER
                    + " " + Build.MODEL + " " + Build.VERSION.RELEASE
                    + " " + Build.VERSION_CODES::class.java.fields[android.os.Build.VERSION.SDK_INT].name)


    override fun toString(): String {
        return "appVersion " + appVersion + " Device ID " + deviceID + " OS " + osVersion + " API " + apiVersion + " device " + deviceInfo
    }
}