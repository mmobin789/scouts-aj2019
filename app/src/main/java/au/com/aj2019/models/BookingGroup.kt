package au.com.aj2019.models

import com.google.gson.annotations.SerializedName

data class BookingGroup(

//        @field:SerializedName("error_message")
//        val errorMessage: Any? = null,

        @field:SerializedName("data")
        val data: Data

//        @field:SerializedName("error")
//        val error: Any? = null,

//        @field:SerializedName("errors")
//        val errors: List<Any?>? = null
) {

    data class Data(

            @field:SerializedName("activity_bookings")
            val activityBookings: List<ActivityBooking>?
    ) {
        data class ActivityBooking(
                @field:SerializedName("activity_slot")
                val activitySlot: Slot,

                @field:SerializedName("group_id")
                val groupId: Int,

                @field:SerializedName("created_at")
                val createdAt: String,

                @field:SerializedName("id")
                val id: Int)


    }
}