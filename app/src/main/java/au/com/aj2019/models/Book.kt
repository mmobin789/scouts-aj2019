package au.com.aj2019.models

/**
 * Created by macbook on 02/02/2018.
 */
data class Book(val rotationID: Int, val activityID: Int, val activityPeriodID: Int)
