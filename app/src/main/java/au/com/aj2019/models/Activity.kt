package au.com.aj2019.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Activity(

        @field:SerializedName("updated_at")
        val updatedAt: String,

        @field:SerializedName("activity_site_id")
        val activitySiteId: Int,

        @field:SerializedName("name")
        val name: String,

        @field:SerializedName("description")
        val description: String,

        @field:SerializedName("created_at")
        val createdAt: String,

        @field:SerializedName("id")
        val id: Int
) : Parcelable