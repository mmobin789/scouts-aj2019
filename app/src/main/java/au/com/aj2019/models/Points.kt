package au.com.aj2019.models

import com.google.gson.annotations.SerializedName


data class Points(

        @field:SerializedName("error_message")
        val errorMessage: String?,

        @field:SerializedName("data")
        val data: Data,

        @field:SerializedName("error")
        val error: String?

//        @field:SerializedName("errors")
//        val errors: List<Any?>? = null
) {
    data class Data(

            @field:SerializedName("bank_account")
            val bankAccount: BankAccount
    ) {
        data class BankAccount(

                @field:SerializedName("group_id")
                val groupId: Int,

                @field:SerializedName("current_balance")
                val currentBalance: Int,

                @field:SerializedName("id")
                val id: Int,

                @field:SerializedName("transactions")
                val transactions: List<Transaction> = emptyList()
        ) {
            data class Transaction(

                    @field:SerializedName("bank_account_id")
                    val bankAccountId: Int,

                    @field:SerializedName("amount")
                    val amount: Int,

                    @field:SerializedName("description")
                    val description: String,

                    @field:SerializedName("current_balance")
                    val currentBalance: Int,

                    @field:SerializedName("created_at")
                    val createdAt: String,

                    @field:SerializedName("id")
                    val id: Int
            )

        }
    }
}