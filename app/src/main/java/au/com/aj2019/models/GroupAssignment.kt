package au.com.aj2019.models


import com.google.gson.annotations.SerializedName

open class GroupAssignment {
    @field:SerializedName("data")
    val data: List<AssignmentItem> = emptyList()


    inner class AssignmentItem private constructor() : GroupAssignment() {

        @field:SerializedName("groupPeriodActivityAssignment_periodID")
        val groupPeriodActivityAssignmentPeriodID = -1

        @field:SerializedName("groupPeriodActivityAssignment_activityID")
        val groupPeriodActivityAssignmentActivityID = -1

        @field:SerializedName("updated_at")
        val updatedAt = ""

        @field:SerializedName("groupPeriodActivityAssignment_note")
        val groupPeriodActivityAssignmentNote = ""

        @field:SerializedName("created_at")
        val createdAt = ""

        @field:SerializedName("deleted_at")
        val deletedAt = ""
        @field:SerializedName("groupPeriodActivityAssignment_groupID")
        val groupPeriodActivityAssignmentGroupID = -1
    }
}