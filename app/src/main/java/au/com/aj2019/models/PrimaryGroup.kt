package au.com.aj2019.models

import au.com.aj2019.utilities.AppPersistence
import com.google.gson.annotations.SerializedName

/**
 * Created by JoupleDesigner on 3/28/2018.
 */
class PrimaryGroup {
    @SerializedName("group_id")
    val id = AppPersistence.userPrimaryGroupID
}