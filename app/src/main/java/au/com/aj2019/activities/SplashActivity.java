package au.com.aj2019.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import au.com.aj2019.R;
import au.com.aj2019.utilities.AppPersistence;

public class SplashActivity extends Base {

    Button btnLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // sharedPreferences = getSharedPreferences("", 0);
        Typeface bold = Typeface.createFromAsset(getAssets(), "Gotham-Black.otf");
        btnLogin = findViewById(R.id.btn_Login);
        btnLogin.setTypeface(bold);
        if (!AppPersistence.INSTANCE.isUserLoggedIn(this)) {
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnLogin.setBackgroundResource(R.drawable.circle_fill);
                    btnLogin.setTextColor(getResources().getColor(R.color.colorPrimary));
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        } else {
            btnLogin.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent in = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(in);
                    finish();
                }
            }, 3000);

        }

    }
}
