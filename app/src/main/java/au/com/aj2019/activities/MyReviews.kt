package au.com.aj2019.activities

import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import au.com.aj2019.R
import au.com.aj2019.adapters.AppViewHolder
import au.com.aj2019.adapters.ReviewsAdapter
import au.com.aj2019.interfaces.OnListItemClickListener
import au.com.aj2019.interfaces.OnRatingListener
import au.com.aj2019.interfaces.OnReviewsListener
import au.com.aj2019.models.Rating
import au.com.aj2019.models.Review
import au.com.aj2019.utilities.AppPersistence
import kotlinx.android.synthetic.main.activity_my_reviews.*
import kotlinx.android.synthetic.main.reviews_adapter.*

class MyReviews : Base(), OnReviewsListener {

    // lateinit var listener: OnListItemClickListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_reviews)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        showUI()
    }

    private fun showUI() {
        val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        //   val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")
        toolbar_title.typeface = light
        textTitle.typeface = bold
        textday.typeface = bold
        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)
        rv.layoutManager = LinearLayoutManager(this)
        rv.visibility = View.GONE
        apiScout.getReviews(this)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onReviewsListed(reviews: MutableList<Review.Data.ReviewData>) {
        if (reviews.size > 0) {
            val listener = object : OnListItemClickListener {
                override fun onItemClick(position: Int, holder: AppViewHolder) {
                    val submit = holder.send
                    val progress = holder.seekBar.progress
                    val rating = when (progress) {
                        0 -> 1
                        else -> progress + 1
                    }
                    val review = reviews[position]
                    val comment = holder.comment.text.toString()

                    submit.text = "Submitting Review..."
                    val ratingListener = object : OnRatingListener {
                        override fun onRatingSent(reviewData: Review.Data.ReviewData) {

                            submit.text = "Review Submitted"
                            submit.isEnabled = false
                            reviews.removeAt(position)
                            rv.adapter.notifyItemRemoved(position)
                            rv.setItemViewCacheSize(reviews.size)

                            if (reviews.isEmpty()) {
                                errorTV.text = "No Pending Reviews"
                                errorTV.visibility = View.VISIBLE
                                rv.visibility = View.GONE
                            }


                        }

                        override fun onFailed(errorCode: Int) {
                            val error = when (errorCode)  // continue from here
                            { 409 -> {
                                    "Activity Review Already Completed"
                                }
                                403 -> {
                                    "Activity Period not Finished"
                                }
                                else -> {
                                    "Rating must be between 1-5"
                                }

                            }
                            submit.setText(R.string.submit)
                            viewUtils.updateUI(submit, error)
                        }

                    }
                    apiScout.sendReview(ratingListener, review.id, Rating(rating, comment))


                }
            }



            rv.visibility = View.VISIBLE
            rv.setItemViewCacheSize(reviews.size)
            rv.adapter = ReviewsAdapter(reviews, listener)
        } else {
            errorTV.text = "No Pending Reviews"
            errorTV.visibility = View.VISIBLE
        }
        loading.smoothToHide()

    }


    override fun onFailed(errorCode: Int) {
        rv.visibility = View.GONE
        errorTV.visibility = View.VISIBLE
        loading.hide()
        errorTV.text = apiScout.handleError(this, errorCode)
    }

}
