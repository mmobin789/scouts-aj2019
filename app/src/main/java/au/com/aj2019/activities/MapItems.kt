package au.com.aj2019.activities

import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import au.com.aj2019.R
import au.com.aj2019.interfaces.OnMapInfoListener
import au.com.aj2019.models.MapInfo
import au.com.aj2019.utilities.AppPersistence

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_map_items.*


class MapItems : Base(), OnMapReadyCallback {

    override fun onMapReady(p0: GoogleMap?) {
        populateMap(p0!!)
    }

    private fun populateMap(map: GoogleMap) {
        apiScout.getMapItems(object : OnMapInfoListener {
            override fun onMapInfo(mapInfo: MapInfo) {
                placeMarkers(map, mapInfo.data)
            }
        })

    }

    private fun placeMarkers(map: GoogleMap, mapInformation: List<MapInfo.DataItem>) {


        for (i in 0 until mapInformation.size) {
            val name = mapInformation[i].name
            val colour = mapInformation[i].colour
            val transparency = mapInformation[i].transparency
            val data = mapInformation[i].coOrdinates
            for (direction in data) {
                val marker = MarkerOptions()
                marker.position(direction)
                marker.title(name)
                marker.snippet(colour + " " + transparency)
                map.addMarker(marker)
                zoomToLocation(map, direction.latitude, direction.longitude)
                Log.i("lat", direction.latitude.toString())
                Log.i("lng", direction.longitude.toString())
            }


        }


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        base = this
        setContentView(R.layout.activity_map_items)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")
        toolbar_title.typeface = light
        textTitle.typeface = bold
        textday.typeface = bold
        textdate.typeface = medium
        initUI()
    }

    private fun initUI() {
        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)
        textdate.text = au.com.aj2019.utilities.Utility.getCurrentTimeStamp()
        val mapFragment = map as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
