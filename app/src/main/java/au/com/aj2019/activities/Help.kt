package au.com.aj2019.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import au.com.aj2019.R
import au.com.aj2019.interfaces.OnLogOutListener
import au.com.aj2019.utilities.AppPersistence
import au.com.aj2019.utilities.Utility
import kotlinx.android.synthetic.main.activity_help.*

class Help : Base(), OnLogOutListener {
    override fun onLogOutSuccess() {
        AppPersistence.clearUser(this)
        progressBar.dismiss()
        setResult(RESULT_OK)
        onBackPressed()
        startActivity(Intent(this, SplashActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        base = this
        setContentView(R.layout.activity_help)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        showUI()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun showUI() {
        val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")
        toolbar_title.typeface = light
        textTitle.typeface = bold
        textday.typeface = bold
        textdate.typeface = medium

        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)
        textdate.text = Utility.getCurrentDate(false)
        device.text = AppPersistence.fireBaseToken
        logout.setOnClickListener({
            progressBar.show()
            apiScout.logOut(this)
        })
    }

}
