package au.com.aj2019.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import au.com.aj2019.R
import au.com.aj2019.utilities.AppPersistence
import kotlinx.android.synthetic.main.activity_plan_do_review.*
import kotlinx.android.synthetic.main.plan_do_review.*

class PlanDoReview : Base(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        base = this
        setContentView(R.layout.activity_plan_do_review)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        showUI()
    }

    private fun showUI() {
        // val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        //  val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")
        toolbar_title.typeface = light
        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)

        layActivity.setOnClickListener(this)
        layPoint.setOnClickListener(this)
        layGeotags.setOnClickListener(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun changeIV(imageView: ImageView) {
        image1.setImageResource(R.drawable.pencil_white)
        image2.setImageResource(R.drawable.tick_white)
        image3.setImageResource(R.drawable.eye_white)
        val res = when (imageView.id) {
            image1.id -> R.drawable.pencil_blue
            image2.id -> R.drawable.tick_blue
            else -> {
                R.drawable.eye_blue
            }
        }
        imageView.setImageResource(res)
    }

    private fun changeTV(textView: TextView) {

        text1.setTextColor(viewUtils.getColor(R.color.white))
        text2.setTextColor(viewUtils.getColor(R.color.white))
        text3.setTextColor(viewUtils.getColor(R.color.white))
        textView.setTextColor(viewUtils.getColor(R.color.colorPrimary))
    }

    private fun changeRL(relativeLayout: RelativeLayout) {
        layActivity.setBackgroundResource(R.drawable.circle)
        layPoint.setBackgroundResource(R.drawable.circle)
        layGeotags.setBackgroundResource(R.drawable.circle)
        relativeLayout.setBackgroundResource(R.drawable.circle_fill)
    }

    override fun onClick(v: View?) {
        changeRL(v as RelativeLayout)
        val activity = when (v.id) {layActivity.id -> {
            changeIV(image1)
            changeTV(text1)
            Activities::class.java

        }
            layPoint.id -> {
                changeIV(image2)
                changeTV(text2)
                Achievements::class.java

            }
            else -> {
                changeIV(image3)
                changeTV(text3)
                MyReviews::class.java
            }
        }
        startActivity(Intent(v.context, activity))

    }

}
