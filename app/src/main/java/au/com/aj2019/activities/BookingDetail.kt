package au.com.aj2019.activities

import android.app.Activity
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import au.com.aj2019.R
import au.com.aj2019.interfaces.OnBookingListener
import au.com.aj2019.models.Booked
import au.com.aj2019.models.Slot
import au.com.aj2019.utilities.AppPersistence
import au.com.aj2019.utilities.Utility
import kotlinx.android.synthetic.main.activities_header.*
import kotlinx.android.synthetic.main.activity_detail.*

class BookingDetail : Base(), View.OnClickListener, OnBookingListener {
    override fun onBookingSuccess(booked: Booked) {
        disableBookingButton(getString(R.string.booked))
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onFailed(errorCode: Int) {
        val error = if (errorCode == 409)
            "Already Booked or Full"
        else "Not Bookable"
        disableBookingButton(error)
    }

    override fun onClick(p0: View?) {

        book.text = "Booking..."
        val slot = getSlot()
        apiScout.bookSlot(slot.activitySiteId, slot.id, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        base = this
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        initUI()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()

    }

    private fun getSlot(): Slot = intent.getParcelableExtra("slot")

    private fun disableBookingButton(text: String) {
        book.setBackgroundColor(Color.LTGRAY)
        book.setTextColor(viewUtils.getColor(R.color.colorPrimary))
        book.text = text
        book.isEnabled = false
    }

    private fun webView(): WebView {
        val settings = mark.settings
        settings.domStorageEnabled = true
        if (!cacheDir.exists())
            cacheDir.mkdirs()

        settings.setAppCachePath(cacheDir.path)
        settings.allowFileAccess = true
        settings.setAppCacheEnabled(true)
        if (Utility.isInternetConnected(this))
            settings.cacheMode = WebSettings.LOAD_DEFAULT
        else settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        return mark
    }

    private fun initUI() {

        val slot = getSlot()
        val activity = slot.activity
        webView().loadData(activity.description, "text/html", "UTF-8")
        val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")
        toolbar_title.typeface = light
        textTitle.typeface = bold
        textday.typeface = bold
        textdate.typeface = medium
        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)
        name.text = activity.name
        detail.text = activity.description
        book.setOnClickListener(this)
        val rotationBooked = intent.getBooleanExtra("rotation_booked", false)

        if (intent.getBooleanExtra("slot_booked", false)) {
            disableBookingButton(getString(R.string.booked))
        } else if (rotationBooked || !slot.bookable) {
            disableBookingButton(getString(R.string.unAvailable))
        }


    }
}
