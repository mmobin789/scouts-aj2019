package au.com.aj2019.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import au.com.aj2019.R
import au.com.aj2019.utilities.AppPersistence
import kotlinx.android.synthetic.main.activity_geo_tags.*

class GeoTags : Base(), View.OnClickListener {
    override fun onClick(view: View) {
        when (view.id) {
            layLogs.id -> {
                changeColorLinear(layLogs)
                changeColorTextView(textLog)
                val intent = Intent(this@GeoTags, QRCode::class.java)
                startActivity(intent)
            }
            layWin.id -> {
                changeColorLinear(layWin)
                changeColorTextView(textWin)

            }
            layUnlock.id -> {
                changeColorLinear(layUnlock)
                changeColorTextView(textUnloack)

            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun changeColorTextView(txtView: TextView) {
        textLog.setTextColor(viewUtils.getColor(R.color.white))
        textWin.setTextColor(viewUtils.getColor(R.color.white))
        textUnloack.setTextColor(viewUtils.getColor(R.color.white))
        txtView.setTextColor(viewUtils.getColor(R.color.colorPrimary))

    }

    private fun changeColorLinear(linView: RelativeLayout) {
        layLogs.setBackgroundResource(R.drawable.circle10)
        layWin.setBackgroundResource(R.drawable.circle10)
        layUnlock.setBackgroundResource(R.drawable.circle10)
        linView.setBackgroundResource(R.drawable.circle_fill10)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_geo_tags)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        showUI()
    }

    private fun showUI() {
        val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")
        toolbar_title.typeface = light
        textTitle.typeface = bold
        textday.typeface = bold
        textdate.typeface = medium
        textLog.typeface = bold
        textWin.typeface = bold
        textUnloack.typeface = bold

        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)

        layLogs.setOnClickListener(this)
        layWin.setOnClickListener(this)
        layUnlock.setOnClickListener(this)
    }

}
