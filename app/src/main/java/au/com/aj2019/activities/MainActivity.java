package au.com.aj2019.activities;

import android.Manifest;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import au.com.aj2019.R;
import au.com.aj2019.utilities.AppPersistence;
import au.com.aj2019.utilities.Utility;

public class MainActivity extends Base implements View.OnClickListener {

    TextView toolbarTitle, txtActivty, txtPointBook, txtGeoTags, txtLibrary, txtAchive, txtHelp;
    RelativeLayout btnActivity, btnPointBook, btnGeoTags, btnLibrary, btnAchive, btnHelp;
    ImageView imgActivity, imgPointBook, imgGeoTags, imgLibrary, imgAchive, imgHelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        base = this;
        setContentView(R.layout.activity_main);
        showUI();
        int PERMISSION_ALL = 999;
        String[] PERMISSIONS = {Manifest.permission.CAMERA};
        if (!Utility.hasPermissions(MainActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, PERMISSION_ALL);
        }
    }


    public void showUI() {
        Typeface bold = Typeface.createFromAsset(getAssets(), "Gotham-Black.otf");
        //  Typeface medium = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        Typeface light = Typeface.createFromAsset(getAssets(), "Gotham-Light.otf");

        toolbarTitle = findViewById(R.id.toolbar_title);
        txtActivty = findViewById(R.id.text1);
        txtPointBook = findViewById(R.id.text2);
        txtGeoTags = findViewById(R.id.text3);
        txtLibrary = findViewById(R.id.text4);
        txtAchive = findViewById(R.id.text5);
        txtHelp = findViewById(R.id.text6);

        btnActivity = findViewById(R.id.layActivity);
        btnPointBook = findViewById(R.id.layPoint);
        btnGeoTags = findViewById(R.id.layGeotags);
        btnLibrary = findViewById(R.id.layLibrary);
        btnAchive = findViewById(R.id.layAchive);
        btnHelp = findViewById(R.id.layHelp);

        imgActivity = findViewById(R.id.image1);
        imgPointBook = findViewById(R.id.image2);
        imgGeoTags = findViewById(R.id.image3);
        imgLibrary = findViewById(R.id.image4);
        imgAchive = findViewById(R.id.image5);
        imgHelp = findViewById(R.id.image6);

        toolbarTitle.setTypeface(light);
        txtActivty.setTypeface(bold);
        txtPointBook.setTypeface(bold);
        txtGeoTags.setTypeface(bold);
        txtLibrary.setTypeface(bold);
        txtAchive.setTypeface(bold);
        txtHelp.setTypeface(bold);
        toolbarTitle.setText(String.format("Hi %s!", AppPersistence.INSTANCE.getName()));
        btnActivity.setOnClickListener(this);
        btnPointBook.setOnClickListener(this);
        btnGeoTags.setOnClickListener(this);
        btnLibrary.setOnClickListener(this);
        btnAchive.setOnClickListener(this);
        btnHelp.setOnClickListener(this);
        // apiScout.sendDeviceInfo();
        apiScout.validateTokenIfRequired(base);
    }


    @Override
    public void onClick(View view) {
        Intent intent;
        if (view == btnActivity) {
            ChangeColorLinear(btnActivity);
            ChangeColorImageView(imgActivity);
            ChangeColorTextView(txtActivty);
            intent = new Intent(MainActivity.this, PlanDoReview.class);
            startActivity(intent);

        } else if (view == btnPointBook) {
            ChangeColorLinear(btnPointBook);
            ChangeColorImageView(imgPointBook);
            ChangeColorTextView(txtPointBook);
            intent = new Intent(MainActivity.this, PointsBook.class);
            startActivity(intent);
        } else if (view == btnGeoTags) {
            ChangeColorLinear(btnGeoTags);
            ChangeColorImageView(imgGeoTags);
            ChangeColorTextView(txtGeoTags);
            intent = new Intent(MainActivity.this, GeoTags.class);
            startActivity(intent);
        } else if (view == btnLibrary) {
            ChangeColorLinear(btnLibrary);
            ChangeColorImageView(imgLibrary);
            ChangeColorTextView(txtLibrary);
            intent = new Intent(MainActivity.this, Library.class);
            startActivity(intent);
        } else if (view == btnAchive) {
            ChangeColorLinear(btnAchive);
            ChangeColorImageView(imgAchive);
            ChangeColorTextView(txtAchive);
            intent = new Intent(MainActivity.this, Achievements.class);
            startActivity(intent);
        } else if (view == btnHelp) {
            ChangeColorLinear(btnHelp);
            ChangeColorImageView(imgHelp);
            ChangeColorTextView(txtHelp);
            intent = new Intent(MainActivity.this, Help.class);
            startActivityForResult(intent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            onBackPressed();
            startActivity(new Intent(this, SplashActivity.class));
        }
    }

    public void ChangeColorTextView(TextView txtView) {
        txtActivty.setTextColor(getResources().getColor(R.color.white));
        txtPointBook.setTextColor(getResources().getColor(R.color.white));
        txtGeoTags.setTextColor(getResources().getColor(R.color.white));
        txtLibrary.setTextColor(getResources().getColor(R.color.white));
        txtAchive.setTextColor(getResources().getColor(R.color.white));
        txtHelp.setTextColor(getResources().getColor(R.color.white));
        txtView.setTextColor(getResources().getColor(R.color.colorPrimary));

    }

    public void ChangeColorImageView(ImageView imageView) {

        imgActivity.setImageDrawable(getResources().getDrawable(R.drawable.ic_star));
        imgPointBook.setImageDrawable(getResources().getDrawable(R.drawable.ic_point_book));
        imgGeoTags.setImageDrawable(getResources().getDrawable(R.drawable.ic_geo_tag));
        imgLibrary.setImageDrawable(getResources().getDrawable(R.drawable.ic_library));
        imgAchive.setImageDrawable(getResources().getDrawable(R.drawable.ic_achive));
        imgHelp.setImageDrawable(getResources().getDrawable(R.drawable.ic_help));
        if (imageView == imgActivity) {
            imgActivity.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_blue));
        } else if (imageView == imgPointBook) {
            imgPointBook.setImageDrawable(getResources().getDrawable(R.drawable.ic_point_book_blue));
        } else if (imageView == imgGeoTags) {
            imgGeoTags.setImageDrawable(getResources().getDrawable(R.drawable.ic_geo_tag_blue));
        } else if (imageView == imgLibrary) {
            imgLibrary.setImageDrawable(getResources().getDrawable(R.drawable.ic_library_blue));
        } else if (imageView == imgAchive) {
            imgAchive.setImageDrawable(getResources().getDrawable(R.drawable.ic_achive_blue));
        } else if (imageView == imgHelp) {
            imgHelp.setImageDrawable(getResources().getDrawable(R.drawable.ic_help_blue));
        }


    }

    public void ChangeColorLinear(RelativeLayout linView) {
        btnActivity.setBackgroundResource(R.drawable.circle);
        btnPointBook.setBackgroundResource(R.drawable.circle);
        btnGeoTags.setBackgroundResource(R.drawable.circle);
        btnLibrary.setBackgroundResource(R.drawable.circle);
        btnAchive.setBackgroundResource(R.drawable.circle);
        btnHelp.setBackgroundResource(R.drawable.circle);
        linView.setBackgroundResource(R.drawable.circle_fill);

    }


}
