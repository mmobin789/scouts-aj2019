package au.com.aj2019.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import au.com.aj2019.R;
import au.com.aj2019.interfaces.OnLoginListener;
import au.com.aj2019.models.Auth;
import au.com.aj2019.utilities.AppPersistence;
import au.com.aj2019.utilities.AsteriskPasswordTransformationMethod;
import au.com.aj2019.utilities.Utility;

public class LoginActivity extends Base implements OnLoginListener {
    public String Tag = "LoginActivity";
    TextView txt1, txt2, txtRegister;
    EditText edtUser, edtPass;
    Button btnLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Typeface bold = Typeface.createFromAsset(getAssets(), "Gotham-Black.otf");
        Typeface medium = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        txt1 = findViewById(R.id.text1);
        txt2 = findViewById(R.id.text2);
        edtUser = findViewById(R.id.edt_userName);
        edtPass = findViewById(R.id.edt_password);
        setSnackBar((RelativeLayout) findViewById(R.id.snackBar));
        //txtRegister = (TextView) findViewById(R.id.txt_register);
        btnLogin = findViewById(R.id.btn_Login);
        txt1.setTypeface(bold);
        txt2.setTypeface(bold);
        btnLogin.setTypeface(bold);
        // txtRegister.setTypeface(medium);
        edtUser.setTypeface(medium);
        edtPass.setTypeface(medium);
        edtPass.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        edtPass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                    login();
                return false;
            }

        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                login();
            }
        });
    }

    private void login() {
        btnLogin.setBackgroundResource(R.drawable.circle_fill);
        btnLogin.setTextColor(getResources().getColor(R.color.colorPrimary));
        if (Utility.isInternetConnected(LoginActivity.this)) {
            if (!edtUser.getText().toString().trim().equals("")) {
                if (!edtPass.getText().toString().trim().equals("")) {
                    progressBar.show();
                    String userName = edtUser.getText().toString().trim();
                    String userPass = edtPass.getText().toString().trim();
                    apiScout.login(new Auth(userName, userPass), LoginActivity.this);
                } else {
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            btnLogin.setBackgroundResource(R.drawable.circle);
                            btnLogin.setTextColor(getResources().getColor(R.color.white));
                        }
                    }, 1000);

                    Toast.makeText(LoginActivity.this, "Please Fill Password", Toast.LENGTH_SHORT).show();
                }
            } else {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        btnLogin.setBackgroundResource(R.drawable.circle);
                        btnLogin.setTextColor(getResources().getColor(R.color.white));
                    }
                }, 1000);
                Toast.makeText(LoginActivity.this, "Please Fill UserName", Toast.LENGTH_SHORT).show();

            }
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    btnLogin.setBackgroundResource(R.drawable.circle);
                    btnLogin.setTextColor(getResources().getColor(R.color.white));
                }
            }, 1000);
            Toast.makeText(LoginActivity.this, "Please Check Internet Connection!", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onLoginSuccess(@NonNull Auth auth) {
        progressBar.dismiss();
        AppPersistence.INSTANCE.saveUser(this, auth);
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLoginFail(@NotNull String error) {
        Log.e("Login_error", error);
        progressBar.dismiss();
        edtUser.setBackgroundResource(R.drawable.red_border);
        edtPass.setBackgroundResource(R.drawable.red_border);
        showSnackBar(this, error, 3);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                edtUser.setBackgroundResource(R.drawable.background_rect_fill);
                edtPass.setBackgroundResource(R.drawable.background_rect_fill);
            }
        }, 3000);
    }
}
