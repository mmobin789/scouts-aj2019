package au.com.aj2019.activities

import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import au.com.aj2019.R
import au.com.aj2019.utilities.AppPersistence
import au.com.aj2019.utilities.Utility
import kotlinx.android.synthetic.main.activity_achievements.*

class Achievements : Base(), View.OnClickListener {
    override fun onClick(view: View) {
        when (view.id) {
            layWood.id -> changeColorImageView(imgWood)
            layMoon.id -> changeColorImageView(imgMoon)
            layTech.id -> changeColorImageView(imgTech)
            layHair.id -> changeColorImageView(imgHair)
            layWater.id -> changeColorImageView(imgWater)
            laySpace.id -> changeColorImageView(imgSpace)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_achievements)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        showUI()
    }

    private fun showUI() {
        val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")

        toolbar_title.typeface = light
        textTitle.typeface = bold
        textday.typeface = bold
        textdate.typeface = medium
        txtStatus.typeface = bold
        txtMyRate.typeface = bold
        txtMyPoint.typeface = bold
        txtWood.typeface = bold
        txtMoon.typeface = bold
        txtTech.typeface = bold
        txtHair.typeface = bold
        txtWater.typeface = bold
        txtSpace.typeface = bold

        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)
        textdate.text = Utility.getCurrentDate(false)

        layWood.setOnClickListener(this)
        layMoon.setOnClickListener(this)
        layTech.setOnClickListener(this)
        layHair.setOnClickListener(this)
        layWater.setOnClickListener(this)
        laySpace.setOnClickListener(this)

    }

    private fun changeColorImageView(imageView: ImageView) {

        imgWood.setImageResource(R.drawable.ic_wood)
        imgMoon.setImageResource(R.drawable.ic_moon)
        imgTech.setImageResource(R.drawable.ic_tech)
        imgHair.setImageResource(R.drawable.ic_hair)
        imgWater.setImageResource(R.drawable.ic_water)
        imgSpace.setImageResource(R.drawable.ic_space)
        if (imageView === imgWood) {
            imgWood.setImageResource(R.drawable.ic_wood_fill)
        } else if (imageView === imgMoon) {
            imgMoon.setImageResource(R.drawable.ic_moon_fill)
        } else if (imageView === imgTech) {
            imgTech.setImageResource(R.drawable.ic_tech_fill)
        } else if (imageView === imgHair) {
            imgHair.setImageResource(R.drawable.ic_hair_fill)
        } else if (imageView === imgWater) {
            imgWater.setImageResource(R.drawable.ic_water_fill)
        } else if (imageView === imgSpace) {
            imgSpace.setImageResource(R.drawable.ic_space_fill)
        }


    }
}
