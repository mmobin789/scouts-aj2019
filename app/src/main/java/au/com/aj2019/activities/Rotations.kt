package au.com.aj2019.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import au.com.aj2019.R
import au.com.aj2019.adapters.RotationsAdapter
import au.com.aj2019.interfaces.OnGroupBookingListener
import au.com.aj2019.interfaces.OnRotationsListener
import au.com.aj2019.models.ActivityPeriodGroup
import au.com.aj2019.models.BookingGroup
import au.com.aj2019.models.RotationGroup
import au.com.aj2019.models.UserSlot
import au.com.aj2019.utilities.AppPersistence
import au.com.aj2019.utilities.Utility
import kotlinx.android.synthetic.main.activities_header.*
import kotlinx.android.synthetic.main.activity_activities.*
import kotlinx.android.synthetic.main.activity_site.*

class Rotations : Base(), OnRotationsListener, OnGroupBookingListener {
    private var rotations: List<RotationGroup.Data.ActivityRotation>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        base = this
        setContentView(R.layout.activity_activities)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        initUI()
    }

//    private fun getFormattedDate(period: ActivityPeriodGroup.Data.ActivityPeriodGroupsItem.ActivityPeriod): String {
//        val start = Utility.getServerDateFormatted(period.startAt)
//        val end = Utility.getServerDateFormatted(period.endAt)
//        return start.substring(start.length - 4, start.length) + "-" + end.substring(end.length - 4, end.length)
//    }

    private fun activitySite() = intent.getParcelableExtra<ActivityPeriodGroup.Data.ActivityPeriodGroupsItem.ActivityPeriod.ActivitySite>("site")

    private fun period() = intent.getParcelableExtra<ActivityPeriodGroup.Data.ActivityPeriodGroupsItem.ActivityPeriod>("period")

    private fun initUI() {
        val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")
        toolbar_title.typeface = light
        textTitle.typeface = bold
        textday.typeface = bold
        textdate.typeface = medium
        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)
        site.visibility = View.VISIBLE
        val period = period()
        textdate.text = Utility.getServerDateFormatted(intent.getStringExtra("date"), false)
        val activitySite = activitySite()
        name.text = activitySite.name
        val timeSlot = Utility.getServerTimeStamp(period.startAt, true) + " - " + Utility.getServerTimeStamp(period.endAt, true)
        date.text = timeSlot
        rv.layoutManager = LinearLayoutManager(this)
        getRotations()


    }

    private fun getRotations() {
        // root.setBackgroundColor(viewUtils.getColor(R.color.colorPrimary))
        loading.smoothToShow()
        rv.visibility = View.GONE
        apiScout.getRotations(activitySite().id, period().id, this)
    }


    override fun onFailed(errorCode: Int) {
        errorTV.visibility = View.VISIBLE
        loading.smoothToHide()
        errorTV.text = apiScout.handleError(this, errorCode)
    }

    override fun onUserRotations(activityRotations: List<RotationGroup.Data.ActivityRotation>) {
        rotations = activityRotations
        apiScout.getGroupBookings(period().id, activitySite().id, this)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 4 && resultCode == Activity.RESULT_OK) {
            getRotations()
        }
    }

    override fun onBookings(activityBookings: List<BookingGroup.Data.ActivityBooking>) {
        rv.visibility = View.VISIBLE
        rv.adapter = RotationsAdapter(filterBookedSlots(activityBookings), this)
        loading.smoothToHide()
    }

    private fun filterBookedSlots(activityBookings: List<BookingGroup.Data.ActivityBooking>): List<UserSlot> {
        val userSlots = mutableListOf<UserSlot>()
        for (rotation in rotations!!) {
            val userSlot = UserSlot(rotation)
            var booked: BookingGroup.Data.ActivityBooking? = null
            for (booking in activityBookings) {

                val activityRotation = booking.activitySlot.rotation
                if (activityRotation.id == rotation.id) {
                    // booking exists for the rotation
                    booked = booking
                    userSlot.rotationHasBooking = true
                    // rv.setBackgroundColor(Color.TRANSPARENT)
                    break
                }
            }
            val slots = rotation.activitySlots
            if (slots != null) {
                for (slot in slots) {
                    if (booked != null) {
                        if (slot.id == booked.activitySlot.id)
                            slot.isBooked = true // show tick for booked
                    }
                }
                //   else         // show default view as it is
                userSlot.activitySlots = slots
            }


            userSlots.add(userSlot)


        }

        return userSlots
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
