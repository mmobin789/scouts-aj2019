package au.com.aj2019.activities

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import au.com.aj2019.R
import au.com.aj2019.utilities.AppPersistence
import au.com.aj2019.utilities.Utility
import kotlinx.android.synthetic.main.activity_announcements.*

class Announcements : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_announcements)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        showUI()
    }

    private fun showUI() {
        val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")
        toolbar_title.typeface = light
        textTitle.typeface = bold
        textday.typeface = bold
        textdate.typeface = medium

        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)
        textdate.text = Utility.getCurrentDate(false)
    }

    override fun onSupportNavigateUp(): Boolean {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
        startActivity(intent)
        finish()
        return true
    }
}