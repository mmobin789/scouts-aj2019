package au.com.aj2019.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import au.com.aj2019.R;
import au.com.aj2019.utilities.AppPersistence;
import au.com.aj2019.utilities.Utility;

public class Library extends Base implements View.OnClickListener {

    public String Tag = "Library";
    TextView toolbarTitle;
    TextView txtTitle;
    TextView txtDay;
    TextView txtDate;
    TextView txtAnnoCount, txtMsgCount, txtNewCount, txtMapCount, txtAnno, txtMyMessage, txtNews, txtMap;
    ImageView imgAnno, imgMsg, imgNews, imgMap;
    RelativeLayout btnAnno, btnMsg, btnNews, btnMap, btnAnnoCount, btnMsgCount, btnNewsCount, btnMapCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        base = this;
        setContentView(R.layout.activity_library);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home);
        showUI();

    }

    public void showUI() {
        Typeface bold = Typeface.createFromAsset(getAssets(), "Gotham-Black.otf");
        Typeface medium = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        Typeface light = Typeface.createFromAsset(getAssets(), "Gotham-Light.otf");
        toolbarTitle = findViewById(R.id.toolbar_title);
        txtTitle = findViewById(R.id.textTitle);
        txtDay = findViewById(R.id.textday);
        txtDate = findViewById(R.id.textdate);
        txtAnnoCount = findViewById(R.id.txtCount);
        txtMsgCount = findViewById(R.id.txtCount1);
        txtNewCount = findViewById(R.id.txtCount2);
        txtMapCount = findViewById(R.id.txtCount3);
        txtAnno = findViewById(R.id.txtMyPoint);
        txtMyMessage = findViewById(R.id.txtMyMessage);
        txtNews = findViewById(R.id.txtNews);
        txtMap = findViewById(R.id.txtMap);

        imgAnno = findViewById(R.id.image1);
        imgMsg = findViewById(R.id.image2);
        imgNews = findViewById(R.id.image3);
        imgMap = findViewById(R.id.image4);

        btnAnno = findViewById(R.id.layAnno);
        btnMsg = findViewById(R.id.layMessage);
        btnNews = findViewById(R.id.layNew);
        btnMap = findViewById(R.id.layMaps);
        btnAnnoCount = findViewById(R.id.lay1);
        btnMsgCount = findViewById(R.id.lay2);
        btnNewsCount = findViewById(R.id.lay3);
        btnMapCount = findViewById(R.id.lay4);


        toolbarTitle.setTypeface(light);
        txtTitle.setTypeface(bold);
        txtDay.setTypeface(bold);
        txtDate.setTypeface(medium);
        txtAnnoCount.setTypeface(bold);
        txtMsgCount.setTypeface(bold);
        txtNewCount.setTypeface(bold);
        txtMapCount.setTypeface(bold);
        txtAnno.setTypeface(bold);
        txtMyMessage.setTypeface(bold);
        txtNews.setTypeface(bold);
        txtMap.setTypeface(bold);

        toolbarTitle.setText(String.format("Hi %s!", AppPersistence.INSTANCE.getName()));
        txtDate.setText(Utility.getCurrentTimeStamp());

        btnAnno.setOnClickListener(this);
        btnMsg.setOnClickListener(this);
        btnNews.setOnClickListener(this);
        btnMap.setOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        if (view == btnAnno) {
            ChangeColorLinear(btnAnno);
            ChangeColorImageView(imgAnno);
            intent = new Intent(Library.this, Announcements.class);
            startActivity(intent);
        } else if (view == btnMsg) {
            ChangeColorLinear(btnMsg);
            ChangeColorImageView(imgMsg);
        } else if (view == btnNews) {
            ChangeColorLinear(btnNews);
            ChangeColorImageView(imgNews);
        } else if (view == btnMap) {
            ChangeColorLinear(btnMap);
            ChangeColorImageView(imgMap);
            startActivity(new Intent(view.getContext(), MapItems.class));

        }
    }

    public void ChangeColorImageView(ImageView imageView) {

        imgAnno.setImageDrawable(getResources().getDrawable(R.drawable.ic_anno));
        imgMsg.setImageDrawable(getResources().getDrawable(R.drawable.ic_message));
        imgNews.setImageDrawable(getResources().getDrawable(R.drawable.ic_news));
        imgMap.setImageDrawable(getResources().getDrawable(R.drawable.ic_map));

        if (imageView == imgAnno) {
            imgAnno.setImageDrawable(getResources().getDrawable(R.drawable.ic_anno_fill));
        } else if (imageView == imgMsg) {
            imgMsg.setImageDrawable(getResources().getDrawable(R.drawable.ic_message_fill));
        } else if (imageView == imgNews) {
            imgNews.setImageDrawable(getResources().getDrawable(R.drawable.ic_news_fill));
        } else if (imageView == imgMap) {
            imgMap.setImageDrawable(getResources().getDrawable(R.drawable.ic_map_fill));
        }

    }

    public void ChangeColorLinear(RelativeLayout linView) {
        btnAnno.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle));
        btnMsg.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle));
        btnNews.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle));
        btnMap.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle));
        linView.setBackgroundDrawable(getResources().getDrawable(R.drawable.circle_fill));

    }
}
