package au.com.aj2019.activities

import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import au.com.aj2019.R
import au.com.aj2019.adapters.PointsAdapter
import au.com.aj2019.interfaces.OnPointsListener
import au.com.aj2019.models.Points
import au.com.aj2019.utilities.AppPersistence
import au.com.aj2019.utilities.Utility
import kotlinx.android.synthetic.main.activity_point_book.*


class PointsBook : Base(), OnPointsListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        base = this
        setContentView(R.layout.activity_point_book)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        initUI()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun initUI() {
        val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")
        rv.layoutManager = LinearLayoutManager(this)

        toolbar_title.typeface = light
        textTitle.typeface = bold
        textday.typeface = bold
        textdate.typeface = medium
        txtPatrol.typeface = bold
        txtPatrolRate.typeface = bold
        txtPatrolPoint.typeface = bold
        txtWhat.typeface = bold
        txtWhen.typeface = bold
        txtValue.typeface = bold
        txtBal.typeface = bold

        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)
        textdate.text = Utility.getCurrentDate(false)
        progressBar.show()
        getPoints()
    }


    private fun getPoints() {
        progressBar.show()
        rv.visibility = View.GONE
        apiScout.getPoints(this)
    }

    override fun onPoints(points: Points) {
        rv.visibility = View.VISIBLE
        txtPatrolRate.text = points.data.bankAccount.currentBalance.toString()
        val transactions = points.data.bankAccount.transactions
        rv.adapter = PointsAdapter(transactions)
        progressBar.dismiss()
    }

    override fun onFailed(errorCode: Int) {
        progressBar.dismiss()
        errorTV.visibility = View.VISIBLE
        errorTV.text = apiScout.handleError(this, errorCode)
    }
}
