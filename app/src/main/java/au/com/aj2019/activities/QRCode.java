package au.com.aj2019.activities;

import android.content.Intent;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import org.jetbrains.annotations.Nullable;

import au.com.aj2019.R;
import au.com.aj2019.interfaces.OnQRCodeResultListener;
import au.com.aj2019.models.QRCodeResult;
import au.com.aj2019.utilities.PointsOverlayView;
import au.com.aj2019.utilities.Utility;

public class QRCode extends Base implements QRCodeReaderView.OnQRCodeReadListener, OnQRCodeResultListener {

    private static final int MY_PERMISSION_REQUEST_CAMERA = 0;
    TextView toolbarTitle;
    TextView txtTitle;
    TextView txtDay;
    TextView txtDate;
    TextView resultTextView;
    CheckBox flashlightCheckBox;
    QRCodeReaderView qrCodeReaderView;
    private PointsOverlayView pointsOverlayView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        base = this;
        setContentView(R.layout.activity_qrcode);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home);
        showUI();


    }

    public void showUI() {
        Typeface bold = Typeface.createFromAsset(getAssets(), "Gotham-Black.otf");
        Typeface medium = Typeface.createFromAsset(getAssets(), "Gotham-Medium.otf");
        Typeface light = Typeface.createFromAsset(getAssets(), "Gotham-Light.otf");

        toolbarTitle = findViewById(R.id.toolbar_title);
        txtTitle = findViewById(R.id.textTitle);
        txtDay = findViewById(R.id.textday);
        txtDate = findViewById(R.id.textdate);
        resultTextView = findViewById(R.id.result_text_view);

        toolbarTitle.setTypeface(light);
        txtTitle.setTypeface(bold);
        txtDay.setTypeface(bold);
        txtDate.setTypeface(medium);

        toolbarTitle.setText("Hi Scouty!");
        boolean result = Utility.checkPermission(QRCode.this);
        if (result) {
            initQRCodeReaderView();
        }
    }


    private void initQRCodeReaderView() {
        qrCodeReaderView = findViewById(R.id.qrdecoderview);
        pointsOverlayView = findViewById(R.id.points_overlay_view);
        flashlightCheckBox = findViewById(R.id.flashlight_checkbox);
        qrCodeReaderView.setOnQRCodeReadListener(this);
        qrCodeReaderView.setAutofocusInterval(2000L);
        qrCodeReaderView.setOnQRCodeReadListener(this);
        qrCodeReaderView.setBackCamera();
        flashlightCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                qrCodeReaderView.setTorchEnabled(isChecked);
            }
        });
        qrCodeReaderView.startCamera();

    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        resultTextView.setText(text);
        pointsOverlayView.setPoints(points);
        apiScout.qrCode(text, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (qrCodeReaderView != null) {
            qrCodeReaderView.startCamera();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (qrCodeReaderView != null) {
            qrCodeReaderView.stopCamera();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(QRCode.this, MainActivity.class);
        startActivity(intent);
        finish();
        return true;
    }

    @Override
    public void onQRCodeResult(@Nullable QRCodeResult qrCodeResult) {
        String message = "No Record Found matching the QR CODE";
        if (qrCodeResult != null)
            message = qrCodeResult.getMessage();
        Toast.makeText(QRCode.this, message, Toast.LENGTH_SHORT).show();
    }
}
