package au.com.aj2019.activities

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import android.widget.TextView
import au.com.aj2019.R
import au.com.aj2019.api.ApiScout
import au.com.aj2019.daggerDI.DaggerStrike
import au.com.aj2019.utilities.ViewUtils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.wang.avi.AVLoadingIndicatorView
import java.util.concurrent.TimeUnit
import javax.inject.Inject


/**
 * Created by macbook on 08/02/2018.
 */
abstract class Base : AppCompatActivity() {


    @Inject
    lateinit var apiScout: ApiScout
    lateinit var progressBar: Dialog
    @Inject
    lateinit var viewUtils: ViewUtils
    lateinit var base: Base
    protected var snackBar: RelativeLayout? = null

    fun zoomToLocation(googleMap: GoogleMap, lat: Double, lng: Double) {

        val latLng = LatLng(lat, lng)
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 18f)
        googleMap.animateCamera(cameraUpdate)

    }

    fun showSnackBar(base: Base, msg: String, seconds: Long) {
        if (snackBar != null) {
            val layout = snackBar
            val textView = snackBar?.getChildAt(0) as TextView
            if (layout?.visibility == View.GONE) {
                layout.visibility = View.VISIBLE
                textView.text = msg
                val slideUp = AnimationUtils.loadAnimation(base, R.anim.slide_in_up)
                layout.startAnimation(slideUp)
                Handler().postDelayed({
                    layout.startAnimation(AnimationUtils.loadAnimation(base, R.anim.slide_out_down))
                    layout.animation.setAnimationListener(object : Animation.AnimationListener {
                        override fun onAnimationStart(animation: Animation) {

                        }

                        override fun onAnimationEnd(animation: Animation) {
                            layout.visibility = View.GONE
                        }

                        override fun onAnimationRepeat(animation: Animation) {

                        }
                    })
                }, TimeUnit.SECONDS.toMillis(seconds))
            }
        } else Log.e("SnackBar", "SnackBar must be initialized in the activity or it must be included in activity layout manually for now")
    }

    private fun createProgressBar(context: Context): Dialog {

        val dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setOnKeyListener { _, _, p2 ->
            if (p2.keyCode == KeyEvent.KEYCODE_BACK) {
                dialog.dismiss()
                onBackPressed()

            }
            true
        }
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val relativeLayout = RelativeLayout(context)
        val avLoadingIndicatorView = AVLoadingIndicatorView(context)
        avLoadingIndicatorView.setIndicatorColor(ContextCompat.getColor(context, R.color.colorAccent))
        //avLoadingIndicatorView.setIndicator(BallPulseIndicator())
        val params = RelativeLayout.LayoutParams(150, 150)
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        avLoadingIndicatorView.layoutParams = params
        relativeLayout.addView(avLoadingIndicatorView)
        dialog.setContentView(relativeLayout)
        return dialog


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerStrike.doInjection(this).inject(this)
        progressBar = createProgressBar(this)


    }


}