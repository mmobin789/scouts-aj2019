package au.com.aj2019.activities

import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import au.com.aj2019.R
import au.com.aj2019.adapters.UserAssignmentsAdapter
import au.com.aj2019.interfaces.OnActivityPeriodGroupsListener
import au.com.aj2019.models.ActivityPeriodGroup
import au.com.aj2019.utilities.AppPersistence
import au.com.aj2019.utilities.Utility
import kotlinx.android.synthetic.main.activities_header.*
import kotlinx.android.synthetic.main.activity_activities.*

class Activities : Base(), OnActivityPeriodGroupsListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        base = this
        setContentView(R.layout.activity_activities)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_home)
        initUI()
    }

    private fun initUI() {
        val bold = Typeface.createFromAsset(assets, "Gotham-Black.otf")
        val medium = Typeface.createFromAsset(assets, "Gotham-Medium.otf")
        val light = Typeface.createFromAsset(assets, "Gotham-Light.otf")
        rv.layoutManager = LinearLayoutManager(this)
        toolbar_title.typeface = light
        textTitle.typeface = bold
        textday.typeface = bold
        textdate.typeface = medium
        toolbar_title.text = String.format("Hi %s!", AppPersistence.name)
        textdate.text = Utility.getCurrentDate(false)
        getUserAssignments()
    }


    private fun getUserAssignments() {
        loading.smoothToShow()
        rv.visibility = View.GONE
        apiScout.getUserAssignments(this)
    }

    override fun onActivityPeriodGroups(activityPeriodGroups: List<ActivityPeriodGroup.Data.ActivityPeriodGroupsItem>) {
        rv.visibility = View.VISIBLE
        val userAssignmentsAdapter = UserAssignmentsAdapter(activityPeriodGroups)
        rv.adapter = userAssignmentsAdapter
        loading.hide()
    }

    override fun onFailed(errorCode: Int) {
        errorTV.visibility = View.VISIBLE
        loading.smoothToHide()
        errorTV.text = apiScout.handleError(this, errorCode)
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
