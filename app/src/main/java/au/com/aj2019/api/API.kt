package au.com.aj2019.api

import au.com.aj2019.models.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by macbook on 01/01/2018.
 */
interface API {
    @POST("auth/login")
    fun login(@Body auth: Auth): Call<Auth>

    @POST
    fun sendDevice(@Header("Authorization") header: String, @Url url: String, @Body device: Device): Call<ResponseBody>

//    @GET
//    fun getGroupActivityPeriodAssignment(@Header("Authorization") header: String, @Url url: String): Call<GroupAssignment>

//    @POST("booking")
//    fun book(@Header("Authorization") header: String, @Body book: Book): Call<Booked>

    @GET
    fun qrCode(@Header("Authorization") header: String, @Url url: String): Call<QRCodeResult>

    @GET("mapItems/GetAll")
    fun getMapItems(@Header("Authorization") header: String): Call<ResponseBody>

    @GET
    fun getPoints(@Header("Authorization") header: String, @Url url: String): Call<Points>

//    @GET("activity-booking/activity-period-groups")
//    fun getActivityPeriodGroups(@Header("Authorization") header: String): Call<ActivityPeriodGroup>

    @GET
    fun getUserAssignments(@Header("Authorization") header: String, @Url url: String): Call<ActivityPeriodGroup>

    @GET
    fun getRotations(@Header("Authorization") header: String, @Url url: String): Call<RotationGroup>

    @POST("auth/logout")
    fun logOut(@Header("Authorization") header: String): Call<ResponseBody>

    @POST("auth/refresh")
    fun refreshToken(@Header("Authorization") header: String): Call<Auth>

    @POST
    fun bookSlot(@Header("Authorization") header: String, @Body primaryGroup: PrimaryGroup, @Url url: String): Call<Booked>

//    @GET
//    fun checkBooking(@Header("Authorization") header: String, @Url url: String): Call<ResponseBody>

    @GET
    fun getBookingGroup(@Header("Authorization") header: String, @Url url: String): Call<BookingGroup>

    @GET
    fun getReviews(@Header("Authorization") header: String, @Url url: String): Call<Review>

    @POST
    fun rate(@Header("Authorization") header: String, @Url url: String, @Body rating: Rating): Call<Review.Data.ReviewData>
}