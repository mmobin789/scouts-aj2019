package au.com.aj2019.api

import android.content.Intent
import android.os.Handler
import android.util.Log
import au.com.aj2019.activities.Base
import au.com.aj2019.activities.LoginActivity
import au.com.aj2019.daggerDI.DaggerStrike
import au.com.aj2019.interfaces.*
import au.com.aj2019.models.*
import au.com.aj2019.utilities.AppPersistence
import au.com.aj2019.utilities.Utility
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Inject


/**
 * Created by macbook on 01/01/2018.
 */
class ApiScout {


//    private var apiScout: ApiScout? = null
//    fun getInstance(): ApiScout {
//        if (apiScout == null) {
//            apiScout = ApiScout()
//
//        }
//
//        return apiScout!!
//    }

    @Inject
    lateinit var retrofit: Retrofit
    @Inject
    lateinit var gson: Gson
    private val baseURL = "http://103.219.145.21/api/v2/"

    init {
        DaggerStrike.doInjection(null).inject(this)
    }


    private fun getAPI(): API {


//
//        if (retrofit == null) {
//
//            val client = OkHttpClient().newBuilder().readTimeout(20, TimeUnit.SECONDS)
//                    .writeTimeout(20, TimeUnit.SECONDS).build()
//
//            retrofit = Retrofit.Builder()
//                    .baseUrl(baseURL)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(client)
//                    .build()
//        }

        return retrofit.create(API::class.java)
    }

    private fun refreshToken(onTokenRefreshListener: OnTokenRefreshListener) {
        getAPI().refreshToken(AppPersistence.token).enqueue(object : Callback<Auth> {
            override fun onFailure(call: Call<Auth>?, t: Throwable?) {
                Log.e("refreshTokenAPI", t.toString())
            }

            override fun onResponse(call: Call<Auth>?, response: Response<Auth>?) {
                val data = response!!.body()
                Log.i("refreshTokenAPI", gson.toJson(data))
                if (data != null)
                    onTokenRefreshListener.onTokenRefreshed(data)
                else onTokenRefreshListener.onFailed(response.code())
            }
        })
    }

    fun sendDeviceInfo() {
        val url = baseURL + "users/" + AppPersistence.uid + "/devices"
        val device = Device(AppPersistence.fireBaseToken)
        Log.i("Device", gson.toJson(device))
        getAPI().sendDevice(AppPersistence.token, url, device).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                Log.e("DeviceAPI", t.toString())
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                Log.i("DeviceAPI", response.toString())
                if (response!!.isSuccessful)
                    Log.i("DeviceAPI", response.body()?.string())
            }
        })
    }

    fun login(auth: Auth, onLoginListener: OnLoginListener) {

        Log.i("loginAPIBody", gson.toJson(auth))
        getAPI().login(auth).enqueue(object : Callback<Auth> {
            override fun onResponse(call: Call<Auth>?, response: Response<Auth>?) {
                val a = response?.body()
                Log.i("LoginAPI", gson.toJson(a) + a?.message)
                Log.i("LoginAPI", response.toString())
                if (response!!.isSuccessful)
                    onLoginListener.onLoginSuccess(response.body()!!)
                else
                    onLoginListener.onLoginFail("Wrong Credentials")
            }

            override fun onFailure(call: Call<Auth>?, t: Throwable?) {
                Log.e("LoginAPI", t.toString())
                onLoginListener.onLoginFail(t.toString())
            }

        })
    }


    fun qrCode(qrCode: String, onQRCodeResultListener: OnQRCodeResultListener) {
        val url = baseURL + "api/qrcode/scan/" + qrCode
        getAPI().qrCode(AppPersistence.token, url).enqueue(object : Callback<QRCodeResult> {
            override fun onResponse(call: Call<QRCodeResult>?, response: Response<QRCodeResult>?) {
                val a = response?.body()
                Log.i("QrCodeAPI", response.toString())
                Log.i("QrCodeAPI", gson.toJson(a))
                onQRCodeResultListener.onQRCodeResult(a)
            }

            override fun onFailure(call: Call<QRCodeResult>?, t: Throwable?) {
                Log.e("QrCodeAPI", t.toString())
            }

        })
    }

    fun getMapItems(onMapInfoListener: OnMapInfoListener) {
        getAPI().getMapItems(AppPersistence.token).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                val a = response?.body()
                val jsonObject = JSONObject(a?.string())
                Log.i("MapItemsAPI", jsonObject.toString())
                val dataArray = jsonObject.getJSONArray("data")
                val dataItems = mutableListOf<MapInfo.DataItem>()

                for (i in 0 until dataArray.length()) {

                    val data = dataArray[i] as JSONObject
                    if (data.get("coords") is JSONArray) {
                        val locations = mutableListOf<LatLng>()
                        val lats = mutableListOf<String>()
                        val longs = mutableListOf<String>()
                        val coords = data.getJSONArray("coords")
                        for (j in 0 until coords.length()) {
                            val coordsChild = coords.getJSONArray(j)
                            for (k in 0 until coordsChild.length()) {
                                val coordsGrandChild = coordsChild.getJSONArray(k)
                                for (c in 0 until coordsGrandChild.length()) {
                                    val key = coordsGrandChild.getString(c)

                                    if (c + 1 < coordsGrandChild.length()) {
                                        val value = coordsGrandChild.getString(c + 1)

                                        if (key == "lat")
                                            lats.add(value)
                                        else
                                            longs.add(value)
                                    }

                                }


                            }

                        }
                        (0 until lats.size).mapTo(locations) { LatLng(lats[it].toDouble(), longs[it].toDouble()) }
                        val dataItem = MapInfo.DataItem(data.getString("colour"), data.getInt("transparency"), data.getString("name"), locations)
                        dataItems.add(dataItem)
                        Log.i("location", locations.size.toString())
                        Log.i("longs", longs.size.toString())
                        Log.i("lats", lats.size.toString())
                        //  lats.clear()
                        //  longs.clear()
                        //  locations.clear()

                    }
                }
                val mapInfo = MapInfo(dataItems, jsonObject.getString("message"), jsonObject.getString("errors"), jsonObject.getBoolean("status"))

                onMapInfoListener.onMapInfo(mapInfo)

            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                Log.e("MapItemsAPI", t.toString())
            }
        })
    }

    //
//    fun getActivityListing(onActivitiesListener: OnActivitiesListener) {
//        getAPI().getActivities(AppPersistence.token).enqueue(object : Callback<Activity> {
//            override fun onResponse(call: Call<Activity>?, response: Response<Activity>?) {
//                val a = response?.body()
//                Log.i("activityListingsAPI", response.toString())
//                Log.i("activityListingAPI", gson.toJson(a))
//                if (response!!.isSuccessful)
//                    onActivitiesListener.onActivitiesList(a?.data)
//            }
//
//            override fun onFailure(call: Call<Activity>?, t: Throwable?) {
//                Log.e("activityListingsAPI", t.toString())
//            }
//        })
//    }
    fun validateTokenIfRequired(base: Base) {  // Run this function once only its recursive and stays operational for entire App lifecycle.
        // It checks token every 5 minutes to see if expired then performs auto refresh on it.
        if (AppPersistence.token.length > 10) {
            val expiryDate = Utility.getServerDateFormatted(AppPersistence.expiry, true)
            val currentDate = Utility.getCurrentDate(true)
            val expiryTime = Utility.getServerTimeStamp(AppPersistence.expiry, false)
            val currentTime = Utility.getCurrentTimeStamp()
            val expiry = expiryTime.split(":")
            val current = currentTime.split(":")
            val currentHour = if (current[0].substring(0) == "0")
                current[0].substring(1).toInt()
            else
                current[0].toInt()
            val currentMinutes = if (current[1].substring(0) == "0")
                current[1].substring(1).toInt()
            else current[1].toInt()
            val expiryHour = if (expiry[0].substring(0) == "0")
                expiry[0].substring(1).toInt()
            else
                expiry[0].toInt()
            val expiryMinutes = if (expiry[1].substring(0) == "0")
                expiry[1].substring(1).toInt()
            else expiry[1].toInt()
            Log.i("tokenExpiryTime", expiryTime)
            Log.i("tokenExpiryDate", expiryDate)
            Log.i("currentDate", currentDate)
            Log.i("currentTime", currentTime)
            val expiryConditions = expiryDate == currentDate && currentHour >= expiryHour

            when {
                expiryConditions -> {
                    Log.i("tokenStatus", "Expired")
                    refreshToken(object : OnTokenRefreshListener {
                        override fun onFailed(errorCode: Int) {
                            Log.e("refreshTokenAPI", "Token must be refreshed before expiry not after it")
                            handleError(base, errorCode)
                        }

                        override fun onTokenRefreshed(auth: Auth) {
                            AppPersistence.saveUser(base, auth)

                        }
                    })

                }

                else -> Log.i("tokenStatus", "Valid")
            }
            Handler().postDelayed({
                validateTokenIfRequired(base)

            }, TimeUnit.MINUTES.toMillis(5))
        }

    }

    fun bookSlot(siteID: Int, slotID: Int, onBookingListener: OnBookingListener) {
        val url = baseURL + "activity-booking/activity-sites/" + siteID + "/slots/" + slotID + "/bookings"
        getAPI().bookSlot(AppPersistence.token, PrimaryGroup(), url).enqueue(object : Callback<Booked> {
            override fun onFailure(call: Call<Booked>?, t: Throwable?) {
                Log.e("bookingAPI", t.toString())
            }

            override fun onResponse(call: Call<Booked>?, response: Response<Booked>?) {
                Log.i("BookingAPI", response.toString())
                if (response!!.isSuccessful) {
                    val data = response.body()
                    Log.i("BookingAPI", gson.toJson(data))
                    onBookingListener.onBookingSuccess(data!!)
                } else onBookingListener.onFailed(response.code())
            }
        })
    }
//
//    fun checkBooking(rotationID: Int) {
//        val url = baseURL + "groups/" + AppPersistence.userPrimaryGroupID + "/activity-booking/bookings?rotation=" + rotationID
//        getAPI().checkBooking(AppPersistence.token, url).enqueue(object : Callback<ResponseBody> {
//            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
//                Log.i("checkBookingAPI", response?.body()?.string())
//                Log.i("checkBookingAPI", response?.toString())
//            }
//
//            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
//                Log.e("checkBookingAPI", t.toString())
//            }
//        })
//    }

//    fun book(book: Book, onBookingListener: OnBookingListener) {
//        getAPI().book(AppPersistence.token, book).enqueue(object : Callback<Booked> {
//            override fun onFailure(call: Call<Booked>?, t: Throwable?) {
//                Log.e("BookingAPI", t.toString())
//            }
//
//            override fun onResponse(call: Call<Booked>?, response: Response<Booked>?) {
//                Log.i("BookingAPI", gson.toJson(response?.body()))
//                onBookingListener.onBookingSuccess(response?.body()!!)
//            }
//        })
//    }

//    fun getGroupActivityPeriodAssignment() {
//        val url = baseURL + "api/groupActivityPeriodAssignments-by-groupid/" + AppPersistence.userPrimaryGroupID
//
//        getAPI().getGroupActivityPeriodAssignment(AppPersistence.token, url).enqueue(object : Callback<GroupAssignment> {
//            override fun onFailure(call: Call<GroupAssignment>?, t: Throwable?) {
//                Log.e("groupActivityAPI", Log.getStackTraceString(t))
//
//            }
//
//            override fun onResponse(call: Call<GroupAssignment>?, response: Response<GroupAssignment>?) {
//                if (response!!.isSuccessful) {
//                    Log.i("groupActivityAPI", gson.toJson(response.body()))
//                    //  AppPersistence.groupAssignment = response.body()!!
//                }
//
//            }
//        })
//    }

    fun getPoints(onPointsListener: OnPointsListener) {
        val url = baseURL + "groups/" + AppPersistence.userPrimaryGroupID + "/banking/account"

        getAPI().getPoints(AppPersistence.token, url).enqueue(object : Callback<Points> {
            override fun onFailure(call: Call<Points>?, t: Throwable?) {
                Log.e("PointsAPI", t.toString())
            }

            override fun onResponse(call: Call<Points>?, response: Response<Points>?) {
                if (response!!.isSuccessful) {
                    Log.i("PointsAPI", gson.toJson(response.body()))
                    if (response.isSuccessful)
                        onPointsListener.onPoints(response.body()!!)
                    else onPointsListener.onFailed(response.code())
                }
            }

        })

    }

//    fun getActivityPeriods(onActivitiesPeriodListener: OnActivitiesPeriodListener) {
//        getAPI().getActivityPeriods(AppPersistence.token).enqueue(object : Callback<ActivityPeriod> {
//            override fun onResponse(call: Call<ActivityPeriod>?, response: Response<ActivityPeriod>?) {
//                val a = response?.body()
//                Log.i("activityPeriodsAPI", response.toString())
//                Log.i("activityPeriodsAPI", gson.toJson(a))
//                if (response!!.isSuccessful)
//                    onActivitiesPeriodListener.onActivitiesPeriodsList(response.body()?.data)
//            }
//
//            override fun onFailure(call: Call<ActivityPeriod>?, t: Throwable?) {
//                Log.e("activityPeriodsAPI", t.toString())
//                onActivitiesPeriodListener.onError(t.toString())
//            }
//
//
//        })
//    }
//
//    fun getActivityPeriodGroups(onActivityPeriodGroupsListener: OnActivityPeriodGroupsListener) {
//        getAPI().getActivityPeriodGroups(AppPersistence.token).enqueue(object : Callback<ActivityPeriodGroup> {
//            override fun onFailure(call: Call<ActivityPeriodGroup>?, t: Throwable?) {
//                Log.e("ActivityPeriodGroupsAPI", t.toString())
//            }
//
//            override fun onResponse(call: Call<ActivityPeriodGroup>?, response: Response<ActivityPeriodGroup>?) {
//                val v = response?.body()
//                Log.i("ActivityPeriodGroupsAPI", gson.toJson(v))
//                onActivityPeriodGroupsListener.onActivityPeriodGroups(v?.data!!.activityPeriodGroups)
//            }
//        })
//    }

    fun getRotations(siteID: Int, periodID: Int, onRotationsListener: OnRotationsListener) {
        val url = baseURL + "activity-booking/activity-sites/" + siteID + "/slots?period=" + periodID + "&group-by=rotations"
        getAPI().getRotations(AppPersistence.token, url).enqueue(object : Callback<RotationGroup> {
            override fun onFailure(call: Call<RotationGroup>?, t: Throwable?) {
                Log.e("RotationsAPI", t.toString())
            }

            override fun onResponse(call: Call<RotationGroup>?, response: Response<RotationGroup>?) {
                val data = response?.body()
                Log.i("RotationsAPI", response.toString())
                Log.i("RotationsAPI", gson.toJson(data))
                if (data != null)
                    onRotationsListener.onUserRotations(data.data.activityRotations)
                else onRotationsListener.onFailed(response!!.code())
            }
        })
    }

    fun logOut(onLogOutListener: OnLogOutListener) {
        getAPI().logOut(AppPersistence.token).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                Log.e("LogOutAPI", t.toString())
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                Log.i("LogOutAPI", response.toString())
                if (response?.code() == 200)
                    onLogOutListener.onLogOutSuccess()

            }
        })
    }


    fun getGroupBookings(periodID: Int, siteID: Int, onGroupBookingListener: OnGroupBookingListener) {
        val url = baseURL + "groups/" + AppPersistence.userPrimaryGroupID + "/activity-booking/bookings?period=" + periodID + "&site=" + siteID
        getAPI().getBookingGroup(AppPersistence.token, url).enqueue(object : Callback<BookingGroup> {
            override fun onFailure(call: Call<BookingGroup>?, t: Throwable?) {
                Log.e("BookingsApi", t.toString())
            }

            override fun onResponse(call: Call<BookingGroup>?, response: Response<BookingGroup>?) {
                val data = response!!.body()
                if (data != null) {

                    Log.i("BookingsAPI", response.toString())
                    Log.i("BookingsAPI", gson.toJson(data))
                    if (data.data.activityBookings != null)
                        onGroupBookingListener.onBookings(data.data.activityBookings)
                    else onGroupBookingListener.onFailed(response.code())
                } else {
                    onGroupBookingListener.onFailed(response.code())
                }
            }
        })
    }

    fun getUserAssignments(onActivityPeriodGroupsListener: OnActivityPeriodGroupsListener) {
        val url = baseURL + "groups/" + AppPersistence.userPrimaryGroupID + "/activity-booking/assignments"
        getAPI().getUserAssignments(AppPersistence.token, url).enqueue(object : Callback<ActivityPeriodGroup> {
            override fun onFailure(call: Call<ActivityPeriodGroup>?, t: Throwable?) {
                Log.e("userAssignmentsAPI", t.toString())

            }

            override fun onResponse(call: Call<ActivityPeriodGroup>?, response: Response<ActivityPeriodGroup>?) {
                val data = response?.body()
                Log.i("userAssignmentsAPI", gson.toJson(data))
                if (data != null)
                    onActivityPeriodGroupsListener.onActivityPeriodGroups(data.data.activityPeriodGroups)
                else onActivityPeriodGroupsListener.onFailed(response!!.code())

            }
        })
    }

    fun getReviews(onReviewsListener: OnReviewsListener) {
        val url = baseURL + "users/" + AppPersistence.uid + "/activity-booking/reviews?review-open=true&review-completed=false"
        getAPI().getReviews(AppPersistence.token, url).enqueue(object : Callback<Review> {
            override fun onFailure(call: Call<Review>?, t: Throwable?) {
                Log.e("getReviewsAPI", t.toString())
            }

            override fun onResponse(call: Call<Review>?, response: Response<Review>?) {
                val data = response!!.body()
                if (data != null)
                    onReviewsListener.onReviewsListed(data.data.reviews.toMutableList())
                else onReviewsListener.onFailed(response.code())
            }
        })
    }

    fun sendReview(onRatingListener: OnRatingListener, reviewID: Int, rating: Rating) {
        val url = baseURL + "users/" + AppPersistence.uid + "/activity-booking/reviews/" + reviewID
        getAPI().rate(AppPersistence.token, url, rating).enqueue(object : Callback<Review.Data.ReviewData> {
            override fun onFailure(call: Call<Review.Data.ReviewData>?, t: Throwable?) {
                Log.e("RatingsAPI", t.toString())
            }

            override fun onResponse(call: Call<Review.Data.ReviewData>?, response: Response<Review.Data.ReviewData>?) {
                if (response!!.isSuccessful) {
                    val data = response.body()!!
                    Log.i("RatingsAPI", gson.toJson(data))
                    onRatingListener.onRatingSent(data)
                } else onRatingListener.onFailed(response.code())
            }
        })
    }

    fun handleError(base: Base, errorCode: Int): String {
        return if (errorCode >= 500 || errorCode == 401) {
            AppPersistence.clearUser(base)
            base.finish()
            base.startActivity(Intent(base, LoginActivity::class.java))
            "Login Required"
        } else if (errorCode == 403) {
            "Forbidden"
        } else {
            if (!Utility.isInternetConnected(base))
                "Please Check your Internet."
            else "Network Issue. Please try again later."
        }

    }
}

