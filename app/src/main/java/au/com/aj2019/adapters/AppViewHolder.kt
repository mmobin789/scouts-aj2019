package au.com.aj2019.adapters

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import au.com.aj2019.R
import au.com.aj2019.activities.Base
import au.com.aj2019.activities.BookingDetail
import au.com.aj2019.activities.Rotations
import au.com.aj2019.daggerDI.DaggerStrike
import au.com.aj2019.interfaces.OnListItemClickListener
import au.com.aj2019.models.*
import au.com.aj2019.utilities.Utility
import au.com.aj2019.utilities.ViewUtils
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.activity_content.*
import kotlinx.android.synthetic.main.activity_rotation_adapter.*
import kotlinx.android.synthetic.main.points_adapter.*
import kotlinx.android.synthetic.main.reviews_adapter.*
import kotlinx.android.synthetic.main.user_assigments_adapter.*
import javax.inject.Inject


/**
 * Created by macbook on 02/01/2018.
 */
class AppViewHolder(override val containerView: View, listener: OnListItemClickListener?, listType: ListType?) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    @Inject
    lateinit var viewUtils: ViewUtils

    init {
        DaggerStrike.doInjection(context = containerView.context).inject(this)
        when (listType) {
            ListType.slots -> {
                containerView.setOnClickListener {
                    listener?.onItemClick(adapterPosition, this)
                }
            }
            ListType.user -> {

            }
            ListType.activities -> {

            }
            ListType.rotations -> {


            }
            ListType.assignments -> {
                //  lineTV.visibility = View.VISIBLE
                name.typeface = Typeface.DEFAULT_BOLD
                contentPanel.visibility = View.GONE


            }
            ListType.reviews -> {
                send.setOnClickListener {
                    listener?.onItemClick(adapterPosition, this)
                }
            }
        }

    }

    //    fun show(activityInfo: ActivityInfo) {
//        dateTime.text = Utility.getServerDateFormatted(activityInfo.activityPeriodStartTimeFiltered)
//        rv.layoutManager = LinearLayoutManager(dateTime.context)
//        val adapter = RotationsAdapter(activityInfo.periodicActivities)
//        rv.adapter = adapter
//        adapter.setOnItemClickListener(this)
//
//    }
    enum class ListType {
        rotations, assignments, activities, reviews, user, slots
    }

    fun bind(transaction: Points.Data.BankAccount.Transaction) {
        val desc = if (transaction.description.isBlank())
            containerView.context.getString(R.string.na)
        else transaction.description
        txtWhat.text = desc
        txtWhen.text = Utility.getFullServerTimestampFormatted(transaction.createdAt)
        txtValue.text = viewUtils.getFormattedAmount(transaction.amount)
        val balance = "Balance " + viewUtils.getFormattedAmount(transaction.currentBalance)
        txtBal.text = balance

        if (transaction.currentBalance > 0) {
            txtBal.setTextColor(Color.WHITE)
        } else {
            txtBal.setTextColor(Color.RED)
        }

    }


    fun bind(activityPeriodGroupsItem: ActivityPeriodGroup.Data.ActivityPeriodGroupsItem) {
        dateTimeTV.text = Utility.getServerDateFormatted(activityPeriodGroupsItem.startAt, false)
        rvPeriods.layoutManager = LinearLayoutManager(containerView.context)
        rvPeriods.adapter = ActivityPeriodsAdapter(activityPeriodGroupsItem.activityPeriods, object : OnListItemClickListener {
            override fun onItemClick(position: Int, holder: AppViewHolder) {
                val period = activityPeriodGroupsItem.activityPeriods[position]
                val detail = Intent(containerView.context, Rotations::class.java)
                detail.putExtra("period", period)
                detail.putExtra("date", activityPeriodGroupsItem.startAt)
                if (period.activitySite != null) {
                    detail.putExtra("site", period.activitySite)
                    containerView.context.startActivity(detail)
                } else {
                    updateUI(holder.contentPanel)
                }
            }
        })
    }

    private fun hideView() {
        containerView.layoutParams = RecyclerView.LayoutParams(0, 0)

    }

    private fun updateUI(textView: TextView) {
        textView.typeface = Typeface.DEFAULT_BOLD
        Handler().postDelayed({
            textView.typeface = Typeface.DEFAULT
        }, 3000)
    }

    fun bind(activityPeriod: ActivityPeriodGroup.Data.ActivityPeriodGroupsItem.ActivityPeriod) {
        name.text = activityPeriod.name
        if (activityPeriod.activitySite != null)
            contentPanel.text = activityPeriod.activitySite.name
        else {
            hideView()
        }
    }

    fun bind(userSlot: UserSlot, base: Base) {
        this.rotation.text = userSlot.rotation.name
        val slots = userSlot.activitySlots
        if (slots != null && slots.isNotEmpty()) {
            line.visibility = View.GONE
            rv.layoutManager = LinearLayoutManager(containerView.context)
            val slotsAdapter = SlotsAdapter(slots, userSlot.rotationHasBooking)
            rv.adapter = slotsAdapter
            slotsAdapter.setOnItemClickListener(object : OnListItemClickListener {
                override fun onItemClick(position: Int, holder: AppViewHolder) {
                    val slot = slots[position]
                    val detail = Intent(containerView.context, BookingDetail::class.java)
                    detail.putExtra("rotation_booked", userSlot.rotationHasBooking)
                    detail.putExtra("slot_booked", slot.isBooked)
                    detail.putExtra("slot", slot)
                    base.startActivityForResult(detail, 4)

                }
            })

        } else {
            hideView()
//            rv.visibility = View.GONE
        }

    }


    fun bind(slot: Slot, rotationHasBooking: Boolean) {

        if (rotationHasBooking) {
            if (slot.isBooked) {
                booked.visibility = View.VISIBLE

            } else {
                containerView.setBackgroundColor(viewUtils.getColor(R.color.disabled_primary))
                name.setTextColor(viewUtils.getColor(R.color.disabled_text))
                contentPanel.setTextColor(viewUtils.getColor(R.color.disabled_spaces))
            }
        }

        name.text = slot.activity.name
        val slotData = slot.bookedSlots.toString() + "/" + slot.allocatedSlots + " SPACES"
        contentPanel.text = slotData
    }

    fun bind(reviewData: Review.Data.ReviewData) {
        text.text = reviewData.activitySlot.activity.name
    }


}


