package au.com.aj2019.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import au.com.aj2019.R
import au.com.aj2019.interfaces.OnListItemClickListener
import au.com.aj2019.models.Slot


/**
 * Created by macbook on 02/02/2018.
 */
class SlotsAdapter(private val list: List<Slot>, private val isRotationHasBooking: Boolean) : RecyclerView.Adapter<AppViewHolder>() {

    private var listener: OnListItemClickListener? = null


    fun setOnItemClickListener(listener: OnListItemClickListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {
        return AppViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.activity_content, parent, false), listener, AppViewHolder.ListType.slots)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {
        holder.bind(list[position], isRotationHasBooking)
    }
}