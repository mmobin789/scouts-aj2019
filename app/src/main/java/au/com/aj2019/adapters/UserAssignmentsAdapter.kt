package au.com.aj2019.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import au.com.aj2019.R
import au.com.aj2019.interfaces.OnListItemClickListener
import au.com.aj2019.models.ActivityPeriodGroup


/**
 * Created by JoupleDesigner on 3/12/2018.
 */
class UserAssignmentsAdapter(private val activityPeriodGroups: List<ActivityPeriodGroup.Data.ActivityPeriodGroupsItem>) : RecyclerView.Adapter<AppViewHolder>() {
    private var onListItemClickListener: OnListItemClickListener? = null

    constructor(activityPeriodGroups: List<ActivityPeriodGroup.Data.ActivityPeriodGroupsItem>, onListItemClickListener: OnListItemClickListener) : this(activityPeriodGroups) {
        this.onListItemClickListener = onListItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {
        return AppViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_assigments_adapter, parent, false), onListItemClickListener, AppViewHolder.ListType.user)
    }

    override fun getItemCount(): Int {
        return activityPeriodGroups.size
    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {
        holder.bind(activityPeriodGroups[position])
    }
}