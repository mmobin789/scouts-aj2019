package au.com.aj2019.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import au.com.aj2019.R
import au.com.aj2019.activities.Base
import au.com.aj2019.interfaces.OnListItemClickListener
import au.com.aj2019.models.UserSlot


/**
 * Created by macbook on 02/01/2018.
 */
class RotationsAdapter(private val list: List<UserSlot>, private val rotationsUI: Base) : RecyclerView.Adapter<AppViewHolder>() {

    private var listener: OnListItemClickListener? = null


    fun setOnItemClickListener(listener: OnListItemClickListener) {
        this.listener = listener
    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {
        val data = list[position]
        holder.bind(data, rotationsUI)
        //   holder?.show(activityInfo)
//        when (getItemViewType(position)) {
//            0 -> {
//
//                holder?.showActivityPeriods(activityInfo as ActivityPeriod.Data)
//
//
//            }
//            else -> {
//
//                holder?.showActivities(activityInfo as Activity.Data)
//            }
//        }
    }


    override fun getItemCount(): Int {
        return list.size
    }

//    override fun getItemViewType(position: Int): Int {
//        val activitiesInfo = list[position]
//        return if (activitiesInfo is ActivityPeriod.Data)
//            0
//        else
//            1
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {

        return AppViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.activity_rotation_adapter, parent, false), listener, AppViewHolder.ListType.activities)
//        return when (viewType) {
//            1 ->
//                AppViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.activity_content, parent, false), listener, AppViewHolder.ListType.activity)
//            else -> AppViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.apg_adapter, parent, false), null, AppViewHolder.ListType.period)
//
//        }


    }


}