package au.com.aj2019.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import au.com.aj2019.R
import au.com.aj2019.interfaces.OnListItemClickListener
import au.com.aj2019.models.ActivityPeriodGroup


/**
 * Created by JoupleDesigner on 3/12/2018.
 */
class ActivityPeriodsAdapter(private val activityPeriods: List<ActivityPeriodGroup.Data.ActivityPeriodGroupsItem.ActivityPeriod>, private val onListItemClickListener: OnListItemClickListener) : RecyclerView.Adapter<AppViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {
        return AppViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.activity_content, parent, false), onListItemClickListener, AppViewHolder.ListType.slots)

    }

    override fun getItemCount(): Int {
        return activityPeriods.size
    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {
        holder.bind(activityPeriods[position])
    }

}