package au.com.aj2019.adapters

import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import au.com.aj2019.R
import au.com.aj2019.interfaces.OnListItemClickListener
import au.com.aj2019.models.Review
import kotlinx.android.synthetic.main.reviews_adapter.*

class ReviewsAdapter(private val reviews: List<Review.Data.ReviewData>, private val onListItemClickListener: OnListItemClickListener) : RecyclerView.Adapter<AppViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {
        val holder = AppViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.reviews_adapter, parent, false), onListItemClickListener, AppViewHolder.ListType.reviews)
        holder.comment.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val data = (1000 - s.length).toString() + " characters remaining"
                holder.chars.text = data
            }
        })
        return holder

    }

    override fun getItemCount(): Int {
        return reviews.size
    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {
        holder.bind(reviews[position])
    }


}