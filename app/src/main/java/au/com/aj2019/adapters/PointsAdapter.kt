package au.com.aj2019.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import au.com.aj2019.R
import au.com.aj2019.models.Points


/**
 * Created by macbook on 12/02/2018.
 */
class PointsAdapter(private val transactions: List<Points.Data.BankAccount.Transaction>) : RecyclerView.Adapter<AppViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {
        return AppViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.points_adapter, parent, false), null, null)

    }

    override fun getItemCount(): Int {
        return transactions.size
    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {
        holder.bind(transactions[position])
    }
}