package au.com.aj2019.utilities

import android.content.Context
import android.os.Handler
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.widget.TextView
import java.text.NumberFormat
import java.util.*

/**
 * Created by JoupleDesigner on 3/28/2018.
 */
class ViewUtils(private val context: Context) {
    fun getColor(@ColorRes color: Int) =
            ContextCompat.getColor(context, color)

    fun updateUI(textView: TextView, msg: String) {
        val label = textView.text.toString()
        textView.text = msg
        textView.isEnabled = false
        Handler().postDelayed({
            textView.isEnabled = true
            textView.text = label
        }, 3000)
    }

    fun getFormattedAmount(amount: Int): String = NumberFormat.getNumberInstance(Locale.US).format(amount)

}