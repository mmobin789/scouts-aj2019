package au.com.aj2019.utilities

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.widget.TextView
import au.com.aj2019.R


/**
 * Created by macbook on 01/01/2018.
 */
class StrokeTextView(context: Context?, attrs: AttributeSet?) : TextView(context, attrs) {
    private var strikeEnabled = false
    private var paint: Paint? = null
//    private var xLine = 0f
//    private var xLinePlus = 0f
//    private var yLine = 0f
//    private var yLinePlus = 0f


    init {
        paint = Paint()
        val typedArray = context?.obtainStyledAttributes(attrs, R.styleable.customTextView)
        setStrikeThroughEnabled(typedArray!!.getBoolean(R.styleable.customTextView_setStrikeThroughEnabled, false))
        setStrikeThroughColor(typedArray.getColor(R.styleable.customTextView_setStrikeThroughColor, Color.BLACK))
        setStrikeThroughLineHeight(typedArray.getFloat(R.styleable.customTextView_setStrikeThroughLineHeight, resources.displayMetrics.density))
//        setStrokeLineStartX(typedArray.getFloat(R.styleable.customTextView_setStrikeLineStartX, 0f))
//        setStrokeLineStartY(typedArray.getFloat(R.styleable.customTextView_setStrikeLineStartY, height / 2.toFloat()))
//        setStrokeLineStopX(typedArray.getFloat(R.styleable.customTextView_setStrikeLineStopX, width.toFloat()))
//        setStrokeLineStopY(typedArray.getFloat(R.styleable.customTextView_setStrikeLineStopY, height / 2.toFloat()))
        typedArray.recycle()
    }

    fun setStrikeThroughEnabled(strikeThroughEnabled: Boolean) {
        strikeEnabled = strikeThroughEnabled
        if (!strikeEnabled)
            Log.e(javaClass.simpleName, "Strike through needs to be enabled 1st by invoking call setStrikeThroughEnabled by xml or Java/Kotlin")
    }

    fun setStrikeThroughColor(color: Int) {
        paint?.color = color
    }

    fun setStrikeThroughLineHeight(lineHeight: Float) {
        paint?.strokeWidth = lineHeight
    }

    fun setStrikeThroughColor(color: String) {


        paint?.color = Color.parseColor(color)
    }

//    fun setStrokeLineStartX(float: Float) {
//        xLinePlus = float
//    }
//
//    fun setStrokeLineStartY(float: Float) {
//        yLinePlus = float
//    }
//
//    fun setStrokeLineStopX(float: Float) {
//        xLine = float
//    }
//
//    fun setStrokeLineStopY(float: Float) {
//        yLine = float
//    }

    override fun onDraw(canvas: Canvas?) {
        if (strikeEnabled) {
            paint?.style = Paint.Style.STROKE
            paint?.isStrikeThruText = true
            paint?.flags = Paint.ANTI_ALIAS_FLAG
            canvas?.drawLine(0f, height / 2.toFloat(), width.toFloat(), height / 2.toFloat(), paint)

        }
        super.onDraw(canvas)

    }
}