package au.com.aj2019.utilities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


/**
 * Created by Hemraj on 01/08/2017.
 */

public class Utility {


    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public static boolean isInternetConnected(Activity CurrentActivity) {
        Boolean Connected = false;
        ConnectivityManager connectivity = (ConnectivityManager) CurrentActivity.getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        Log.e("Network is: ", "Connected");
                        Connected = true;
                    } else {
                    }

        } else {
            Log.e("Network is: ", "Not Connected");
//
//            Toast.makeText(CurrentActivity.getApplicationContext(),
//                    "Please Check Your  internet connection",
//                    Toast.LENGTH_LONG).show();
            Connected = false;

        }
        return Connected;


    }

    @NonNull
    private static LocalDateTime getCurrentLocalDateTime() {
        return new LocalDateTime();
    }

    public static String getCurrentTimeStamp() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("hh:mm");
        return getCurrentLocalDateTime().toString(dateTimeFormatter);
    }

    private static DateTimeFormatter getServerFormat() {

        return DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");
    }


    public static String getServerTimeStamp(String timeStamp, boolean hours12) {
        if (!TextUtils.isEmpty(timeStamp)) {
            //    DateTimeFormatter dateTimeFormatter = getServerFormat();
            // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());
            // Date date = dateFormat.parse(timeStamp.substring(0, 19));
            DateTime dateTime = getServerFormat().parseDateTime(timeStamp);
            String pattern = "HH:mm";
            if (hours12)
                pattern = "hh:mm a";

            DateTimeFormatter hourFormatter = DateTimeFormat.forPattern(pattern).withZone(DateTimeZone.getDefault());
            // dateFormat = new SimpleDateFormat("hh:mm", Locale.getDefault());
            return dateTime.toString(hourFormatter);

        }
        return timeStamp;
    }

    public static String getCurrentDate(boolean simpleDate) {
        String pattern = "EEEE dd MMMM, yyyy";
        if (simpleDate)
            pattern = "dd-MM-yyyy";
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(pattern);
        return getCurrentLocalDateTime().toString(dateTimeFormatter);
    }

    public static String getFullServerTimestampFormatted(String timestamp) {
        DateTime dateTime = getServerFormat().parseDateTime(timestamp);
        return dateTime.toString(DateTimeFormat.forPattern("dd-MM-yyyy hh:mm a").withZone(DateTimeZone.getDefault()));
    }

    public static String getServerDateFormatted(String timeStamp, boolean simpleDate) {

        // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());
        Log.i("ServerDate", timeStamp);
        //Date date = dateFormat.parse(timeStamp.substring(0, 19));
        DateTime date = getServerFormat().parseDateTime(timeStamp); //getServerFormat().parseLocalDateTime(timeStamp);

        // dateFormat = new SimpleDateFormat("EEEE dd MMMM, yyyy", Locale.getDefault());
        String pattern = "EEEE dd MMMM, yyyy";
        if (simpleDate)
            pattern = "dd-MM-yyyy";

        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern(pattern).withZone(DateTimeZone.getDefault());
        return date.toString(dateFormatter);


    }

//    public static String unixToDate(String unixTimeStamp) {
//        long unixTime = Long.parseLong(unixTimeStamp);
//        Date date = new Date(unixTime * 1000L);
//
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEEE dd mmmm,yyyy", Locale.getDefault());
//
//        return simpleDateFormat.format(date);
//    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.CAMERA)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
