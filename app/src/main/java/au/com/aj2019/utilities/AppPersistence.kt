package au.com.aj2019.utilities

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Log
import au.com.aj2019.models.Auth

/**
 * Created by macbook on 01/01/2018.
 */

object AppPersistence {


    private const val key_token = "token"
    private const val key_email = "email"
    private const val key_name = "name"
    private const val key_userPrimaryGroupID = ""
    private const val key_tokenExpiry = "expiry"
    private const val key_uid = "id"


    var token = ""
    var email = ""
    var fireBaseToken = "#"
    var name = ""
    var userPrimaryGroupID = -1
    var expiry = ""
    var uid = -1


    fun clearUser(context: Context) {
        getPreferences(context).edit().remove(key_token).apply()
    }

    fun saveUser(context: Context, auth: Auth) {
        val editor = getPreferences(context).edit()
        editor.putString(key_name, auth.data?.user?.first_name)
        editor.putString(key_token, auth.data?.token)
        editor.putString(key_email, auth.data?.user?.email)
        editor.putString(key_tokenExpiry, auth.data?.expiry)
        val primaryGroup = auth.data?.user?.primaryGroup
        if (primaryGroup != null)
            editor.putInt(key_userPrimaryGroupID, primaryGroup.id)
        editor.putInt(key_uid, auth.data?.user?.id!!)
        editor.apply()
        isUserLoggedIn(context)
    }

    private fun updateFireBaseToken(context: Context) {
        fireBaseToken = getPreferences(context).getString("ftoken", "#")

    }

    fun saveFireBaseToken(context: Context, refreshedToken: String) {
        getPreferences(context).edit().putString("ftoken", refreshedToken).apply()
        updateFireBaseToken(context)
    }

    fun isUserLoggedIn(context: Context): Boolean {
        val preferences = getPreferences(context)
        token = "Bearer " + preferences.getString(key_token, "")
        name = preferences.getString(key_name, "")
        email = preferences.getString(key_email, "")
        userPrimaryGroupID = preferences.getInt(key_userPrimaryGroupID, -1)
        expiry = preferences.getString(key_tokenExpiry, "")
        updateFireBaseToken(context)
        uid = getPreferences(context).getInt(key_uid, -1)
        Log.i("token", token)
        Log.i("UserGroupID", userPrimaryGroupID.toString())
        Log.i("UserID", uid.toString())
        //  userName = getPreferences(context).getString(key_name, "");


        return token.length > 10

    }


    private fun getPreferences(context: Context): SharedPreferences {

        return PreferenceManager.getDefaultSharedPreferences(context)

    }

}